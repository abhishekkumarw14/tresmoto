package com.tresmoto.cache;

import com.tresmoto.constants.PaymentGatewayType;
import com.tresmoto.constants.TransactionType;
import com.tresmoto.repository.GatewayCodeToPaymentStatusRepository;
import com.tresmoto.repository.entity.GatewayCodeToPaymentStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
public class GatewayStatusCodeCache {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private GatewayCodeToPaymentStatusRepository gatewayCodeToPaymentStatusRepository;

    private Map<String, GatewayCodeToPaymentStatus> applicationStartupMap = new HashMap<>();
    private Map<String, GatewayCodeToPaymentStatus> reloadMap = new HashMap<>();
    private volatile AtomicBoolean isApplicationStartupMapActive = new AtomicBoolean(false);

    @PostConstruct
    public void loadCache() throws Exception {
        loadCache(applicationStartupMap);
    }

    private void loadCache(Map<String, GatewayCodeToPaymentStatus> internalWalletConfigsMap) throws Exception {
        log.info("loading GatewayStatusCodeToPEStatusConfig cache");
        internalWalletConfigsMap.clear();
        final List<GatewayCodeToPaymentStatus> gatewayStatusCodeToPEStatusConfigs = gatewayCodeToPaymentStatusRepository.findAll();
        Optional.ofNullable(gatewayStatusCodeToPEStatusConfigs).orElseThrow(Exception::new).forEach((gatewayStatusCodeToPEStatusConfig) ->
                internalWalletConfigsMap.put(getKey(gatewayStatusCodeToPEStatusConfig.getTransactionType(),
                        gatewayStatusCodeToPEStatusConfig.getGatewayCode(), gatewayStatusCodeToPEStatusConfig.getGatewayStatusCode()),
                        gatewayStatusCodeToPEStatusConfig));
        boolean currentFlag = isApplicationStartupMapActive.get();
        isApplicationStartupMapActive.compareAndSet(currentFlag, !currentFlag);
        log.info("successfully loaded GatewayStatusCodeToPEStatusConfig cache {}", internalWalletConfigsMap);
    }

    private String getKey(TransactionType transactionType, PaymentGatewayType paymentGatewayType, String gatewayStatusCode) {
        StringJoiner stringJoiner = new StringJoiner("-");
        stringJoiner.add(transactionType.name());
        stringJoiner.add(paymentGatewayType.name());
        stringJoiner.add(gatewayStatusCode);
        return stringJoiner.toString();
    }

    public void reloadCache() {
        try {
            if (isApplicationStartupMapActive.get()) {
                loadCache(reloadMap);
                return;
            }
            loadCache(applicationStartupMap);
        } catch (Exception e) {
            log.error("Failed to reload GatewayStatusCodeToPEStatusConfig Cache {}", e.getMessage());
        }
    }


    public GatewayCodeToPaymentStatus getGatewayStatusCodeToPEStatusConfig(
            TransactionType transactionType, PaymentGatewayType paymentGatewayType, String gatewayStatusCode) {
        return isApplicationStartupMapActive.get()
                ? applicationStartupMap.get(getKey(transactionType, paymentGatewayType, gatewayStatusCode))
                : reloadMap.get(getKey(transactionType, paymentGatewayType, gatewayStatusCode));
    }
}
