package com.tresmoto.utils;

import com.tresmoto.constants.Status;
import com.tresmoto.repository.RefundTransactionDetailsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static com.tresmoto.constants.Status.INVALID;

@Component
public class EventUtils {

    private Logger log = LoggerFactory.getLogger(MapperUtils.class);

    @Value("${event.payment.expire-record-limit:3}")
    private int expireRecordLimit;

    @Autowired
    private RefundTransactionDetailsRepository refundTransactionDetailsRepository;


    public void updateEventStatus(Long id, Date createdDate, String paymentEngineTransactionId, Status status) {
        if (TimeUnit.DAYS.convert(Math.abs(new Date().getTime()
                - createdDate.getTime()), TimeUnit.MILLISECONDS) > expireRecordLimit) {
            log.warn("record was created on {} unsuccessful after multiple attempt of retry for id {} and " +
                    "payment transaction id  {}", createdDate, id, paymentEngineTransactionId);
            updateEventStatus(id, INVALID);
            return;
        }
        updateEventStatus(id, status);
    }

    public void updateEventStatus(Long id, Status status) {
        //refundTransactionDetailsRepository.u(id, status);
    }
}
