package com.tresmoto.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CommonUtils {

    @Value("${app.url.callback}")
    private String appCallbackUrl;

    public String getGatewayCallBackUrl(String callBackUrl, String paymentTransactionId) {
        StringBuilder stringBuilder = new StringBuilder(callBackUrl);
        stringBuilder.append(paymentTransactionId);
        return stringBuilder.toString();
    }

    public String getAppCallBackUrl(String paymentTransactionId) {
        StringBuilder stringBuilder = new StringBuilder(appCallbackUrl);
        stringBuilder.append(paymentTransactionId);
        return stringBuilder.toString();
    }
}
