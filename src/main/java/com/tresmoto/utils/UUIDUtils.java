package com.tresmoto.utils;

import static com.tresmoto.constants.TransactionConstant.PAYMENT_TRANSACTION_PREFIX;
import static com.tresmoto.constants.TransactionConstant.PAYMENT_TRANSACTION_SEPARATOR;

import com.fasterxml.uuid.Generators;
import org.springframework.stereotype.Component;

import java.util.StringJoiner;

@Component
public class UUIDUtils {

    public String getUniqueIdTransaction() {
        return Generators.timeBasedGenerator().generate().toString();
    }

    public String getUniquePaymentTransactionToken() {
        StringJoiner stringJoiner = new StringJoiner(PAYMENT_TRANSACTION_SEPARATOR);
        stringJoiner.add(PAYMENT_TRANSACTION_PREFIX);
        stringJoiner.add(Generators.timeBasedGenerator().generate().toString());
        return stringJoiner.toString();
    }

}
