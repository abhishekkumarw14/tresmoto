package com.tresmoto.utils;

import static com.tresmoto.constants.TemplateConstants.CALLBACK_FAILURE_TEMPLATE;
import static com.tresmoto.constants.TemplateConstants.TENANT_CALLBACK_TEMPLATE;

import static com.tresmoto.constants.ChannelType.*;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.io.ClassPathTemplateLoader;
import com.github.jknack.handlebars.io.TemplateLoader;
import com.tresmoto.client.PaymentResponse;
import com.tresmoto.exception.RedirectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;


@Service("templatingService")
public class TemplatingService {

    private Handlebars handlebarsInline;
    private Handlebars handlebars;

    private TemplateLoader loader = new ClassPathTemplateLoader();

    private Logger log = LoggerFactory.getLogger(this.getClass());

//    @Autowired
//    private MapperUtils mapperUtils;

    @PostConstruct
    public void prepareTemplateEngines() {
        handlebarsInline = new Handlebars();

        loader.setPrefix("/templates");
        loader.setSuffix(".html");
        handlebars = new Handlebars(loader);


        handlebars.registerHelper("equals", new Helper<Object>() {
            @Override
            public Object apply(Object context, Options options) throws IOException {
                return context.equals(options.param(0)) ? options.fn(this) : options.inverse(this);
            }
        });

//        handlebars.registerHelper("isPaymentChannelAPP", (context, options) ->
//                APP.equals(options.hash("channel")) ? options.fn(this) : options.inverse(this)
//        );
//
//        handlebars.registerHelper("getUrlIfRequiredForChannel", (context, options) ->
//                APP.equals(options.hash("channel")) ? "/payment-engine/api/v1/callback/app" : options.hash("callback")
//        );
//
//        handlebars.registerHelper("isNotNull", (context, options) ->
//                options.hash("data") != null ? options.fn(this) : options.inverse(this));
    }


    public String compileTemplate(String templateName, Object data) {
        try {
            Template template = handlebars.compile(templateName);
            return template.apply(data);
        } catch (IOException exp) {
            throw new RedirectionException(exp.getMessage());
        }
    }


    public String compileCallbackDataTemplate(Object paymentResponse) {
        try {
            Template template = handlebars.compile(TENANT_CALLBACK_TEMPLATE);
            return template.apply(paymentResponse);
        } catch (IOException exp) {
            throw new RedirectionException(exp.getMessage());
        }
    }

    public String compileCallbackFailureTemplate(Exception exception) {
        try {
            Template template = handlebars.compile(CALLBACK_FAILURE_TEMPLATE);
            return template.apply(exception);
        } catch (IOException e) {
            return "BaseError";
        }
    }
}
