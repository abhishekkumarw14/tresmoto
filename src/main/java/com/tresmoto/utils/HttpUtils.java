package com.tresmoto.utils;

import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Map;

@Service("HttpUtils")
public class HttpUtils {

    public String buildPathUrl(String url, Map<String, String> map) {
       return UriComponentsBuilder.fromUriString(url).build(map).toString();
    }
}
