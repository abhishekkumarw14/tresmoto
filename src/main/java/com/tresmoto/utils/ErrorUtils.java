package com.tresmoto.utils;

import com.tresmoto.client.BaseError;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class ErrorUtils {


    public BaseError getBaseError(String errorCode, String errorDescription) {
        BaseError baseError = new BaseError();
        baseError.setCode(errorCode);
        baseError.setDescription(errorDescription);
        return baseError;
    }
}
