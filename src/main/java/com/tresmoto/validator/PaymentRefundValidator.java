package com.tresmoto.validator;


import com.tresmoto.exception.OperationNotAllowedException;
import com.tresmoto.exception.PaymentTransactionNotFoundException;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.utils.EventUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigInteger;

import static com.tresmoto.constants.ExceptionCode.OPERATION_NOT_ALLOWED;
import static com.tresmoto.constants.TransactionConstant.INVALID_TRANSACTION_ID;
import static com.tresmoto.constants.Status.FAILED;
import static com.tresmoto.constants.Status.INVALID;

@Component
public class PaymentRefundValidator {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    private EventUtils eventUtils;


    public void validateRefundPayment(
            PaymentTransactionDetails paymentTransactionDetails, String transactionId, Long id) {
        if (null == paymentTransactionDetails) {
            log.error("invalid payment transaction id {}", transactionId);
            throw new PaymentTransactionNotFoundException(INVALID_TRANSACTION_ID);
        } else if (FAILED == paymentTransactionDetails.getStatus()) {
            log.info("payment is failed refund is invalid for payment transaction id {}", transactionId);
            throw new OperationNotAllowedException(OPERATION_NOT_ALLOWED);
        }
    }
}
