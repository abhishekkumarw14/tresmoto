package com.tresmoto.validator;

import static com.tresmoto.constants.RazorpayConstants.INVALID_ORDER_ID;

import com.tresmoto.dto.request.razorpay.RazorPayS2SCallbackData;
import com.tresmoto.exception.PaymentGatewayException;
import org.springframework.stereotype.Component;

@Component
public class RazorPayValidator {
    public void validateRazorPayResponse(RazorPayS2SCallbackData razorPayS2SCallbackData) {
        if (null == razorPayS2SCallbackData || null == razorPayS2SCallbackData.getPayload() ||
                null == razorPayS2SCallbackData.getPayload().getPayment() ||
                null == razorPayS2SCallbackData.getPayload().getPayment().getEntity() ||
                null == razorPayS2SCallbackData.getPayload().getPayment().getEntity().getOrderId()) {
            throw new PaymentGatewayException(INVALID_ORDER_ID);
        }
    }
}
