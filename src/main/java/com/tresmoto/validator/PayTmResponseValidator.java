package com.tresmoto.validator;

import static com.tresmoto.constants.PayTmConstants.*;

import com.tresmoto.dto.request.paytm.PayTmTransactionResponse;
import com.tresmoto.exception.PaymentGatewayException;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;


@Component
public class PayTmResponseValidator {

    public void validatePayTmResponse(MultiValueMap<String, String> params) {
        if (null == params) {
            throw new PaymentGatewayException("INVALID CALL BACK DATA IN PAYTM RESPONSE");
        }
        if (null == params.get(STATUS.name()) ||
                params.get(STATUS.name()).isEmpty()) {
            throw new PaymentGatewayException("TRANSACTION FAILURE INVALID transaction status ");
        }
        if (null == params.get(CHECKSUMHASH.name()) || params.get(CHECKSUMHASH.name()).isEmpty()
                || null == params.get(CHECKSUMHASH.name()).get(0)) {
            throw new PaymentGatewayException(" TRANSACTION FAILURE INVALID CHECKSUMHASH IN RESPONSE");
        }

    }

    public boolean isResponseInvalid(PayTmTransactionResponse paymentStatusResponse) {
        return (null == paymentStatusResponse || null == paymentStatusResponse.getStatus()
                || TXN_FAILURE.name().equalsIgnoreCase(paymentStatusResponse.getStatus()));

    }

    public boolean isStatusPending(PayTmTransactionResponse paymentStatusResponse) {
        return null != paymentStatusResponse && PENDING.name().equalsIgnoreCase(paymentStatusResponse.getStatus());
    }
}
