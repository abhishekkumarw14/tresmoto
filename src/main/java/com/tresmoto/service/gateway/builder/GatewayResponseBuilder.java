package com.tresmoto.service.gateway.builder;

import com.tresmoto.client.PaymentResponse;
import com.tresmoto.client.PaymentRequest;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import org.springframework.http.ResponseEntity;

public interface GatewayResponseBuilder {


    ResponseEntity preparePaymentResponse(PaymentResponse paymentResponse, PaymentRequest paymentRequest,
                                         PaymentTransactionDetails paymentTransactionDetails);
}
