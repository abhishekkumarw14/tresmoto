package com.tresmoto.service.gateway.builder.impl;

import com.tresmoto.client.PaymentRequest;
import com.tresmoto.client.PaymentResponse;
import com.tresmoto.dto.request.paytm.PayTmPaymentResponse;
import com.tresmoto.dto.request.razorpay.RazorPayPaymentResponse;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.service.gateway.builder.GatewayResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component("razorPayResponseBuilder")
public class RazorPayResponseBuilder implements GatewayResponseBuilder {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public ResponseEntity preparePaymentResponse(PaymentResponse paymentResponse, PaymentRequest paymentRequest
            , PaymentTransactionDetails paymentTransactionDetails) {
//        log.debug("BillDeskResponseBuilder.preparePayNowResponse get called for payment transaction id {} and " +
//                "payment instrument is {}", paymentTransactionDetails.getPaymentTransactionId(), paymentRequest.getPaymentMethod());
//        HttpHeaders httpHeaders = new HttpHeaders();
//        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
//        return ResponseEntity.status(HttpStatus.OK).headers(httpHeaders)
//                .body(paymentResponse);

        log.debug("BillDeskResponseBuilder.preparePayNowResponse get called for payment transaction id {} and " +
                "payment instrument is {}", paymentTransactionDetails.getPaymentTransactionId(), paymentRequest.getPaymentMethod());
        RazorPayPaymentResponse payTmPaymentResponse = (RazorPayPaymentResponse) paymentResponse;
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_HTML_VALUE);
        return ResponseEntity.status(HttpStatus.OK).headers(httpHeaders)
                .body(payTmPaymentResponse.getResponseString());

    }
}
