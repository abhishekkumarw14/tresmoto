package com.tresmoto.service.callback;

import com.tresmoto.client.PaymentGatewayCallbackData;
import com.tresmoto.client.PaymentResponse;

public interface CallBackService {

    PaymentResponse processGatewayCallBackData(PaymentGatewayCallbackData paymentGatewayCallbackData);

    void processGatewayS2SCallBackData(PaymentGatewayCallbackData paymentGatewayCallbackData);
}
