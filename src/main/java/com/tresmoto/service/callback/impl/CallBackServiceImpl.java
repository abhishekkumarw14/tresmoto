package com.tresmoto.service.callback.impl;

import static com.tresmoto.constants.TransactionConstant.INVALID_TRANSACTION_ID;
import static com.tresmoto.constants.Status.*;

import com.tresmoto.client.PaymentGatewayCallbackData;
import com.tresmoto.client.PaymentResponse;
import com.tresmoto.exception.PaymentTransactionNotFoundException;
import com.tresmoto.factory.service.PaymentGatewayServiceFactory;
import com.tresmoto.repository.PaymentTransactionDetailsRepository;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.service.callback.CallBackService;
import com.tresmoto.service.payment.PaymentRefundService;
import com.tresmoto.service.payment.PaymentTransactionService;
import com.tresmoto.service.payment.PaymentGatewayService;
import com.tresmoto.service.wallet.PromoWalletService;
import com.tresmoto.service.wallet.UserWalletService;
import com.tresmoto.utils.CommonUtils;
import com.tresmoto.utils.ErrorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CallBackServiceImpl implements CallBackService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PaymentGatewayServiceFactory paymentGatewayServiceFactory;

    @Autowired
    private PaymentTransactionDetailsRepository paymentTransactionDetailsRepository;

    @Autowired
    private ErrorUtils errorUtils;

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private UserWalletService userWalletService;

    @Autowired
    private PromoWalletService promoWalletService;

    @Autowired
    private PaymentRefundService paymentRefundService;

    @Autowired
    private CommonUtils commonUtils;

    @Override
    @Transactional
    public PaymentResponse processGatewayCallBackData(PaymentGatewayCallbackData paymentGatewayCallbackData) {
        PaymentTransactionDetails paymentTransactionDetails = paymentTransactionDetailsRepository
                .findByPaymentTransactionIdForUpdate(paymentGatewayCallbackData.getPaymentTransactionId());
        if (null == paymentTransactionDetails) {
            log.error("invalid payment transaction id {}", paymentGatewayCallbackData.getPaymentTransactionId());
            throw new PaymentTransactionNotFoundException(INVALID_TRANSACTION_ID);
        } else if (PENDING != paymentTransactionDetails.getStatus()) {
            log.info("payment is already processed transaction id {}", paymentGatewayCallbackData.getPaymentTransactionId());
            return paymentResponseForProcessedTransaction(paymentTransactionDetails);
        }
        return processGatewayB2BCallBack(paymentGatewayCallbackData, paymentTransactionDetails);
    }

    @Override
    @Transactional
    public void processGatewayS2SCallBackData(PaymentGatewayCallbackData paymentGatewayCallbackData) {
        PaymentTransactionDetails paymentTransactionDetails = paymentTransactionDetailsRepository
                .findByGatewayCandidateTransactionIdForUpdate(paymentGatewayCallbackData.getPaymentTransactionId());
        if (null == paymentTransactionDetails) {
            log.error("invalid payment transaction id {}", paymentGatewayCallbackData.getPaymentTransactionId());
            throw new PaymentTransactionNotFoundException(INVALID_TRANSACTION_ID);
        }
        processGatewayS2SCallBack(paymentGatewayCallbackData, paymentTransactionDetails);
    }


    private PaymentResponse processGatewayB2BCallBack(PaymentGatewayCallbackData paymentGatewayCallbackData,
                                                      PaymentTransactionDetails paymentTransactionDetails) {
        PaymentGatewayService paymentGatewayService =
                paymentGatewayServiceFactory.getPaymentGatewayService(paymentGatewayCallbackData.getPaymentGatewayType());
        PaymentResponse paymentResponse = paymentGatewayService.processGatewayCallBackData(paymentGatewayCallbackData);
        processCallBack(paymentResponse, paymentTransactionDetails);
        return paymentResponse;
    }

    private void processGatewayS2SCallBack(PaymentGatewayCallbackData paymentGatewayCallbackData, PaymentTransactionDetails paymentTransactionDetails) {
        PaymentGatewayService paymentGatewayService = paymentGatewayServiceFactory.getPaymentGatewayService(
                paymentGatewayCallbackData.getPaymentGatewayType());
        PaymentResponse paymentResponse = paymentGatewayService.processGatewayS2sCallBackData(
                paymentGatewayCallbackData, paymentTransactionDetails.getPaymentTransactionId());
        if (FAILED == paymentTransactionDetails.getStatus() && paymentResponse.getStatus() == SUCCESS) {
            paymentRefundService.initiateRefund(paymentTransactionDetails);
            paymentTransactionService.updatePaymentTransactionDetails(paymentResponse, paymentTransactionDetails);
            return;
        }
        processCallBack(paymentResponse, paymentTransactionDetails);
    }

    private void processCallBack(PaymentResponse paymentResponse, PaymentTransactionDetails paymentTransactionDetails) {
        if (SUCCESS == paymentResponse.getStatus() && PENDING == paymentTransactionDetails.getStatus()) {
            userWalletService.updateUserWallet(paymentTransactionDetails);
            promoWalletService.updateStatusByPaymentTransactionId(paymentResponse.getStatus(), paymentTransactionDetails.getPaymentTransactionId());
        }
        paymentTransactionService.updatePaymentTransactionDetails(paymentResponse, paymentTransactionDetails);
    }

    private PaymentResponse paymentResponseForProcessedTransaction(PaymentTransactionDetails paymentTransactionDetails) {
        PaymentResponse paymentResponse = new PaymentResponse();
        paymentResponse.setStatus(paymentTransactionDetails.getStatus());
        paymentResponse.setPaymentTransactionId(paymentTransactionDetails.getPaymentTransactionId());
        if (SUCCESS != paymentTransactionDetails.getStatus()) {
            paymentResponse.setBaseError(errorUtils.getBaseError(paymentTransactionDetails.getGatewayResponseCode(),
                    paymentTransactionDetails.getGatewayResponseMessage()));
        }
        paymentResponse.setCallBackUrl(commonUtils.getAppCallBackUrl(paymentTransactionDetails.getPaymentTransactionId()));
        return paymentResponse;
    }
}
