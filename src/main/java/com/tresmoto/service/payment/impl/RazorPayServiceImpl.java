package com.tresmoto.service.payment.impl;

import com.tresmoto.cache.GatewayStatusCodeCache;
import com.tresmoto.client.*;
import com.tresmoto.dto.request.razorpay.*;
import com.tresmoto.helper.RazorPayHelper;
import com.tresmoto.provider.RazorPayProvider;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.repository.entity.RefundTransactionDetails;
import com.tresmoto.service.payment.PaymentGatewayService;
import com.tresmoto.utils.ErrorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("razorPayServiceImpl")
public class RazorPayServiceImpl implements PaymentGatewayService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RazorPayProvider razorPayProvider;

    @Autowired
    private RazorPayHelper razorPayHelper;

    @Autowired
    private ErrorUtils errorUtils;

    @Autowired
    private GatewayStatusCodeCache gatewayStatusCodeCache;


    @Override
    public PaymentResponse makePayment(PaymentRequest paymentRequest, String paymentTransactionId) {
        return razorPayHelper.getPaymentResponse(paymentRequest, razorPayProvider.createOrder(razorPayHelper.
                createOrderRequest(paymentRequest, paymentTransactionId), paymentTransactionId), paymentTransactionId);
    }

    @Override
    public PaymentResponse processGatewayCallBackData(PaymentGatewayCallbackData paymentGatewayCallbackData) {
        return razorPayHelper.getPaymentResponse((RazorPayB2BCallbackData) paymentGatewayCallbackData);
    }

    @Override
    public PaymentResponse getPaymentStatus(PaymentTransactionDetails paymentTransactionDetails) {
        RazorPayPaymentStatusResponse razorPayOrderResponse =
                razorPayProvider.getPaymentStatus(
                        paymentTransactionDetails.getGatewayOrderId(), paymentTransactionDetails.getPaymentTransactionId());
        return razorPayHelper.getPaymentResponse(razorPayOrderResponse);
    }

    @Override
    public PaymentRefundResponse refundPayment(PaymentTransactionDetails purchaseTransaction,
                                               RefundTransactionDetails refundTransactionDetails) {
        RazorPayRefundResponse razorPayRefundResponse = razorPayProvider.refundPayment(razorPayHelper.createRefundRequest(
                purchaseTransaction, refundTransactionDetails.getRefundAmount()), purchaseTransaction.getPaymentTransactionId());
        return razorPayHelper.getPaymentRefundResponse(purchaseTransaction, razorPayRefundResponse);
    }


    @Override
    public PaymentResponse processGatewayS2sCallBackData(PaymentGatewayCallbackData paymentGatewayCallbackData
            , String paymentTransactionId) {
        return razorPayHelper.getPaymentResponse((RazorPayS2SCallbackData) paymentGatewayCallbackData, paymentTransactionId);
    }

}
