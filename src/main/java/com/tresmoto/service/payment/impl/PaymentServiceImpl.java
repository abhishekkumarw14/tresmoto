package com.tresmoto.service.payment.impl;

import static com.tresmoto.constants.Status.SUCCESS;
import static com.tresmoto.constants.TransactionConstant.INVALID_TRANSACTION_ID;
import static com.tresmoto.constants.Status.PENDING;

import com.tresmoto.client.PaymentResponse;
import com.tresmoto.client.PaymentRequest;
import com.tresmoto.client.PaymentStatusRequest;
import com.tresmoto.constants.PaymentGatewayType;
import com.tresmoto.exception.PaymentTransactionNotFoundException;
import com.tresmoto.factory.routing.PaymentGatewayRouterFactory;
import com.tresmoto.factory.service.PaymentGatewayServiceFactory;
import com.tresmoto.repository.RefundTransactionDetailsRepository;
import com.tresmoto.repository.PaymentTransactionDetailsRepository;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.service.payment.PaymentGatewayService;
import com.tresmoto.service.payment.PaymentRefundService;
import com.tresmoto.service.payment.PaymentService;
import com.tresmoto.service.wallet.PromoWalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class PaymentServiceImpl implements PaymentService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PaymentGatewayRouterFactory paymentGatewayRouterFactory;

    @Autowired
    private PaymentGatewayServiceFactory paymentGatewayServiceFactory;

    @Autowired
    private PaymentTransactionServiceImpl paymentTransactionService;

    @Autowired
    private PaymentTransactionDetailsRepository paymentTransactionDetailsRepository;

    @Autowired
    private RefundTransactionDetailsRepository refundTransactionDetailsRepository;

    @Autowired
    private PromoWalletService promoWalletService;

    @Autowired
    private PaymentRefundService paymentRefundService;


    @Transactional
    @Override
    public ResponseEntity makePayment(PaymentRequest paymentRequest) {
        PaymentGatewayType paymentGatewayType = paymentGatewayRouterFactory.getPaymentGateway(paymentRequest);
        PaymentTransactionDetails paymentTransactionDetails = paymentTransactionService.
                createPaymentTransactionDetails(paymentRequest, paymentGatewayType);
        paymentTransactionService.savePaymentTransactionDetails(paymentTransactionDetails);
        if (null != paymentRequest.getAmount().getPromotionRequest()) {
            promoWalletService.savePromotionalWallet(paymentRequest, paymentTransactionDetails.getPaymentTransactionId());
        }
        return paymentGatewayServiceFactory.getGatewayResponseBuilder(paymentGatewayType).preparePaymentResponse(
                paymentGatewayServiceFactory.getPaymentGatewayService(paymentGatewayType).makePayment(
                        paymentRequest, paymentTransactionDetails.getPaymentTransactionId()), paymentRequest, paymentTransactionDetails);

    }

    @Override
    @Transactional
    public PaymentResponse getPaymentStatus(PaymentStatusRequest paymentStatusRequest) {
        PaymentTransactionDetails paymentTransactionDetails =
                paymentTransactionDetailsRepository.findByPaymentTransactionId(paymentStatusRequest.getPaymentTransactionId());
        if (null == paymentTransactionDetails) {
            log.info("invalid payment transaction id {}", paymentStatusRequest.getPaymentTransactionId());
            throw new PaymentTransactionNotFoundException(INVALID_TRANSACTION_ID);
        }
        if (paymentStatusRequest.isGatewayStatus()) {
            PaymentGatewayService gatewayService = paymentGatewayServiceFactory.getPaymentGatewayService(paymentTransactionDetails.getPaymentGatewayType());
            PaymentResponse paymentResponse = gatewayService.getPaymentStatus(paymentTransactionDetails);
            if (PENDING == paymentTransactionDetails.getStatus() && paymentResponse.getStatus() == SUCCESS) {
                paymentRefundService.initiateRefund(paymentTransactionDetails);
            }
            paymentTransactionDetailsRepository.updatePaymentStatus(paymentStatusRequest.getId(), paymentResponse.getStatus());
            return paymentResponse;
        }
        PaymentResponse paymentResponse = new PaymentResponse();
        paymentResponse.setStatus(paymentTransactionDetails.getStatus());
        paymentResponse.setPaymentTransactionId(paymentTransactionDetails.getPaymentTransactionId());
        return paymentResponse;
    }

}
