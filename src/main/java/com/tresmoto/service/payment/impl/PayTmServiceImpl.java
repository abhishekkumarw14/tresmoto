package com.tresmoto.service.payment.impl;

import static com.tresmoto.constants.ExceptionCode.PAYMENT_REFUND_EXCEPTION;
import static com.tresmoto.constants.Status.*;
import static com.tresmoto.constants.PayTmConstants.*;

import com.tresmoto.cache.GatewayStatusCodeCache;
import com.tresmoto.client.*;
import com.tresmoto.constants.PaymentGatewayType;
import com.tresmoto.constants.Status;
import com.tresmoto.constants.TransactionType;
import com.tresmoto.dto.request.paytm.*;
import com.tresmoto.exception.PaymentRefundException;
import com.tresmoto.helper.PayTmHelper;
import com.tresmoto.provider.PayTmProvider;
import com.tresmoto.repository.entity.GatewayCodeToPaymentStatus;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.repository.entity.RefundTransactionDetails;
import com.tresmoto.service.payment.PaymentGatewayService;
import com.tresmoto.utils.CommonUtils;
import com.tresmoto.utils.ErrorUtils;
import com.tresmoto.validator.PayTmResponseValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.TreeMap;


@Service("payTmServiceImpl")
public class PayTmServiceImpl implements PaymentGatewayService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PayTmHelper payTmHelper;

    @Autowired
    private PayTmProvider payTmProvider;

    @Autowired
    private PayTmResponseValidator payTmResponseValidator;

    @Autowired
    private ErrorUtils errorUtils;

    @Override
    public PaymentResponse makePayment(PaymentRequest paymentRequest, String paymentTransactionId) {
        PayTmPaymentResponse payTmPaymentResponse = new PayTmPaymentResponse();
        payTmPaymentResponse.setResponseString(
                payTmHelper.getPaymentResponse(paymentRequest, paymentTransactionId));
        return payTmPaymentResponse;
    }

    @Override
    public PaymentResponse processGatewayCallBackData(PaymentGatewayCallbackData paymentGatewayCallbackData) {
        PayTmCallbackData payTmCallbackData = (PayTmCallbackData) paymentGatewayCallbackData;
        TreeMap<String, String> parameters = payTmHelper.getPayTmParams(payTmCallbackData.getParams());
        try {
            payTmResponseValidator.validatePayTmResponse(payTmCallbackData.getParams());
//            if (payTmHelper.verifyCheckSum(parameters, payTmCallbackData.getParams().get(CHECKSUMHASH.name()).get(0))) {
            PayTmTransactionResponse payTmTransactionResponse = payTmProvider.getPaymentStatus(
                    payTmHelper.getPaymentStatusRequest(paymentGatewayCallbackData.getPaymentTransactionId()));
            return payTmHelper.getPaymentResponse(payTmTransactionResponse, paymentGatewayCallbackData.getPaymentTransactionId());
//            } else {
//                log.info("payTm callback checksum value is not matched for transaction id {} ",
//                        paymentGatewayCallbackData.getPaymentTransactionId());
//                PaymentResponse paymentResponse = payTmHelper.getPaymentResponse(parameters, paymentGatewayCallbackData.getPaymentTransactionId());
//                paymentResponse.setStatus(Status.PENDING);
//                paymentResponse.setBaseError(errorUtils.getBaseError(VALIDATE_CHECK_SUM.name(), VALIDATE_CHECK_SUM.getValue()));
//                return paymentResponse;
//            }
        } catch (Exception exp) {
            log.info("exception occurred while PayTmServiceImpl.processGatewayCallBackData {} for payment transaction" +
                    " id {} ", exp.getMessage(), paymentGatewayCallbackData.getPaymentTransactionId());
            PaymentResponse paymentResponse = payTmHelper.getPaymentResponse(parameters, paymentGatewayCallbackData.getPaymentTransactionId());
            paymentResponse.setStatus(FAILED);
            paymentResponse.setBaseError(errorUtils.getBaseError(exp.getMessage(), exp.getMessage()));
            return paymentResponse;
        }

    }

    @Override
    public PaymentResponse getPaymentStatus(PaymentTransactionDetails paymentTransactionDetails) {
        PayTmTransactionResponse payTmTransactionResponse = payTmProvider.getPaymentStatus(
                payTmHelper.getPaymentStatusRequest(paymentTransactionDetails.getPaymentTransactionId()));
        return payTmHelper.getPaymentResponse(payTmTransactionResponse);
    }

    @Override
    public PaymentRefundResponse refundPayment(PaymentTransactionDetails purchaseTransaction, RefundTransactionDetails refundTransactionDetails) {
        PaytmRefundResponse paytmRefundResponse = payTmProvider.refundPayment(
                payTmHelper.createRefundRequest(purchaseTransaction, refundTransactionDetails.getRefundAmount()),
                refundTransactionDetails.getPaymentTransactionId());
        return payTmHelper.getPaymentRefundResponse(purchaseTransaction, paytmRefundResponse);
    }

    @Override
    public PaymentResponse processGatewayS2sCallBackData(PaymentGatewayCallbackData paymentGatewayCallbackData,
                                                         String paymentTransactionId) {
        return null;
    }

}
