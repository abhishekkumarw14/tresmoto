package com.tresmoto.service.payment.impl;

import static com.tresmoto.constants.PaymentConstant.TRES_MOTO;
import static com.tresmoto.constants.Status.PENDING;

import com.tresmoto.client.PaymentRefundEventRequest;
import com.tresmoto.client.PaymentRefundEventResponse;
import com.tresmoto.client.PaymentRefundResponse;
import com.tresmoto.constants.TransactionInitiator;
import com.tresmoto.exception.PaymentRefundException;
import com.tresmoto.factory.service.PaymentGatewayServiceFactory;
import com.tresmoto.repository.RefundTransactionDetailsRepository;
import com.tresmoto.repository.PaymentTransactionDetailsRepository;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.repository.entity.RefundTransactionDetails;
import com.tresmoto.service.payment.PaymentGatewayService;
import com.tresmoto.service.payment.PaymentRefundService;
import com.tresmoto.utils.ErrorUtils;
import com.tresmoto.utils.EventUtils;
import com.tresmoto.validator.PaymentRefundValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class PaymentRefundServiceImpl implements PaymentRefundService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PaymentGatewayServiceFactory paymentGatewayServiceFactory;

    @Autowired
    private PaymentTransactionDetailsRepository paymentTransactionDetailsRepository;

    @Autowired
    private ErrorUtils errorUtils;

    @Autowired
    private EventUtils eventUtils;

    @Autowired
    private PaymentRefundValidator paymentRefundValidator;

    @Autowired
    private RefundTransactionDetailsRepository refundTransactionDetailsRepository;


    @Override
    public PaymentRefundEventResponse refundPayment(PaymentRefundEventRequest request) {
        PaymentTransactionDetails paymentTransactionDetails = paymentTransactionDetailsRepository
                .findByPaymentTransactionId(request.getPaymentTransactionId());
        paymentRefundValidator.validateRefundPayment(paymentTransactionDetails, request.getPaymentTransactionId(), request.getId());
        RefundTransactionDetails refundTransactionDetails = refundTransactionDetailsRepository.findOneById(request.getId());
        PaymentGatewayService paymentGatewayService = paymentGatewayServiceFactory.getPaymentGatewayService(paymentTransactionDetails.getPaymentGatewayType());
        PaymentRefundEventResponse paymentRefundEventResponse = new PaymentRefundEventResponse();
        paymentRefundEventResponse.setPaymentEngineTransactionId(paymentTransactionDetails.getPaymentTransactionId());
        try {
            PaymentRefundResponse paymentRefundResponse = paymentGatewayService.
                    refundPayment(paymentTransactionDetails, refundTransactionDetails);
            paymentRefundResponse.setRefundAmount(refundTransactionDetails.getRefundAmount());
            refundTransactionDetailsRepository.updateRefundStatus(request.getId(), paymentRefundResponse.getStatus(),
                    paymentRefundResponse.getGatewayRefundId(), paymentRefundResponse.getRefundAmount());
            paymentRefundEventResponse.setSuccessFull(true);
        } catch (PaymentRefundException exp) {
            paymentRefundEventResponse.setBaseError(errorUtils.getBaseError(exp.getCode(), exp.getMessage()));
        }
        return paymentRefundEventResponse;
    }

    @Override
    @Transactional
    public void initiateRefund(PaymentTransactionDetails paymentTransactionDetails) {
        refundTransactionDetailsRepository.saveAndFlush(getRefundTransactionDetails(paymentTransactionDetails));
    }

    private RefundTransactionDetails getRefundTransactionDetails(PaymentTransactionDetails paymentTransactionDetails) {
        RefundTransactionDetails refundTransactionDetails = new RefundTransactionDetails();
        refundTransactionDetails.setAmount(paymentTransactionDetails.getAmount());
        refundTransactionDetails.setRefundAmount(paymentTransactionDetails.getAmount());
        refundTransactionDetails.setPaymentGatewayCode(paymentTransactionDetails.getPaymentGatewayType());
        refundTransactionDetails.setPaymentTransactionId(paymentTransactionDetails.getPaymentTransactionId());
        refundTransactionDetails.setPurchaseTransactionDate(paymentTransactionDetails.getCreatedOn());
        refundTransactionDetails.setTransactionInitiator(TransactionInitiator.SELF_INITIATED);
        refundTransactionDetails.setStatus(PENDING);
        refundTransactionDetails.setCreatedBy(TRES_MOTO);
        return refundTransactionDetails;
    }

}
