package com.tresmoto.service.payment.impl;

import static com.tresmoto.constants.PaymentConstant.TRES_MOTO;
import static com.tresmoto.constants.TransactionInitiator.TENANT_INITIATED;

import com.tresmoto.client.PaymentRequest;
import com.tresmoto.client.PaymentResponse;
import com.tresmoto.client.PromotionRequest;
import com.tresmoto.constants.PaymentGatewayType;
import com.tresmoto.constants.Status;
import com.tresmoto.repository.PaymentTransactionDetailsRepository;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.service.payment.PaymentTransactionService;
import com.tresmoto.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


@Service
public class PaymentTransactionServiceImpl implements PaymentTransactionService {

    @Autowired
    private PaymentTransactionDetailsRepository paymentTransactionDetailsRepository;

    @Autowired
    private UUIDUtils uuidUtils;

    @Override
    public PaymentTransactionDetails createPaymentTransactionDetails(PaymentRequest paymentRequest, PaymentGatewayType paymentGatewayType) {
        return getPaymentTransactionDetails(paymentRequest, paymentGatewayType);
    }

    @Override
    @Transactional
    public void savePaymentTransactionDetails(PaymentTransactionDetails paymentTransactionDetails) {
        paymentTransactionDetailsRepository.saveAndFlush(paymentTransactionDetails);
    }

    @Override
    @Transactional
    public void updatePaymentTransactionDetails(PaymentResponse paymentResponse, PaymentTransactionDetails paymentTransactionDetails) {
        paymentTransactionDetails.setGatewayTransactionId(paymentResponse.getGatewayTransactionId());
        paymentTransactionDetails.setGatewayResponseCode(paymentResponse.getGatewayResponseCode());
        paymentTransactionDetails.setGatewayResponseMessage(paymentResponse.getGatewayResponseMessage());
        paymentTransactionDetails.setStatus(paymentResponse.getStatus());
        paymentTransactionDetails.setPaymentSource(paymentResponse.getPaymentSource());
        paymentTransactionDetails.setPaymentMethod(paymentResponse.getPaymentMethod());
        paymentTransactionDetails.setGatewayOrderId(paymentResponse.getGatewayOrderId());
        paymentTransactionDetailsRepository.saveAndFlush(paymentTransactionDetails);
    }


    private PaymentTransactionDetails getPaymentTransactionDetails(PaymentRequest paymentRequest, PaymentGatewayType paymentGatewayType) {
        PaymentTransactionDetails paymentTransactionDetails = new PaymentTransactionDetails();
        Date transactionDate = new Date();
        String transactionId = uuidUtils.getUniquePaymentTransactionToken();
        paymentTransactionDetails.setPaymentTransactionId(transactionId);
        paymentTransactionDetails.setAmount(paymentRequest.getAmount().getAmount());
        if (null != paymentRequest.getTenant()) {
            paymentTransactionDetails.setTenantCode(paymentRequest.getTenant().getCode());
            paymentTransactionDetails.setTenantTransactionId(transactionId);
        }
        PromotionRequest promotionRequest = paymentRequest.getAmount().getPromotionRequest();
        if (null != promotionRequest) {
            paymentTransactionDetails.setPromotionalAmount(promotionRequest.getAmount());
        }
        paymentTransactionDetails.setPaymentType(paymentRequest.getPaymentType());
        paymentTransactionDetails.setUserId(paymentRequest.getUser().getUuid());
        paymentTransactionDetails.setChannelType(paymentRequest.getPaymentChannelInformation().getChannelType());
        paymentTransactionDetails.setAppType(paymentRequest.getPaymentChannelInformation().getAppType());
        paymentTransactionDetails.setStatus(Status.PENDING);
        paymentTransactionDetails.setPaymentMethod(paymentRequest.getPaymentMethod());
        paymentTransactionDetails.setPaymentGatewayType(paymentGatewayType);
        paymentTransactionDetails.setCreatedOn(transactionDate);
        paymentTransactionDetails.setCreatedBy(TRES_MOTO);
        paymentTransactionDetails.setTransactionInitiator(TENANT_INITIATED);
        return paymentTransactionDetails;
    }
}
