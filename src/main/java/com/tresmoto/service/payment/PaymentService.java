package com.tresmoto.service.payment;

import com.tresmoto.client.PaymentRequest;
import com.tresmoto.client.PaymentResponse;
import com.tresmoto.client.PaymentStatusRequest;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import org.springframework.http.ResponseEntity;

public interface PaymentService {

    ResponseEntity makePayment(PaymentRequest paymentRequest);

    PaymentResponse getPaymentStatus(PaymentStatusRequest paymentStatusRequest);

}
