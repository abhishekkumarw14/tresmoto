package com.tresmoto.service.payment;

import com.tresmoto.client.*;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.repository.entity.RefundTransactionDetails;

public interface PaymentGatewayService {

    PaymentResponse makePayment(PaymentRequest paymentRequest, String paymentTransactionId);

    PaymentResponse processGatewayCallBackData(PaymentGatewayCallbackData paymentGatewayCallbackData);

    PaymentResponse processGatewayS2sCallBackData(PaymentGatewayCallbackData paymentGatewayCallbackData,
                                                  String paymentTransactionId);

    PaymentResponse getPaymentStatus(PaymentTransactionDetails purchaseTransaction);

    PaymentRefundResponse refundPayment(PaymentTransactionDetails purchaseTransaction, RefundTransactionDetails refundTransactionDetails);
}
