package com.tresmoto.service.payment;

import com.tresmoto.client.PaymentRefundEventRequest;
import com.tresmoto.client.PaymentRefundEventResponse;
import com.tresmoto.repository.entity.PaymentTransactionDetails;

public interface PaymentRefundService {

    PaymentRefundEventResponse refundPayment(PaymentRefundEventRequest request);

    void initiateRefund(PaymentTransactionDetails paymentTransactionDetails);
}
