package com.tresmoto.service.wallet;

import com.tresmoto.client.CashPointRequest;
import com.tresmoto.client.WalletRequest;

public interface PointWalletService {

    void processWalletById(CashPointRequest walletRequest);
}
