package com.tresmoto.service.wallet.impl;

import com.tresmoto.repository.UserWalletRepository;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.repository.entity.UserWallet;
import com.tresmoto.service.wallet.UserWalletService;
import com.tresmoto.utils.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import static com.tresmoto.constants.PaymentType.*;


@Service
public class UserWalletServiceImpl implements UserWalletService {

    @Autowired
    private UserWalletRepository userWalletRepository;

    @Autowired
    private ObjectMapperUtils objectMapperUtils;


    @Transactional(isolation = Isolation.READ_COMMITTED)
    public UserWallet findByUserId(String userId) {
        return userWalletRepository.findByUserId(userId);
    }

    @Transactional
    public void updateUserWallet(PaymentTransactionDetails paymentTransactionDetails) {
        UserWallet userWallet = userWalletRepository.findByUserId(paymentTransactionDetails.getUserId());
        if (paymentTransactionDetails.getPaymentType() == PAYMENT_WITH_PROMOTION) {
            userWallet.setPromotionalAmount(userWallet.getPromotionalAmount().add(paymentTransactionDetails.getPromotionalAmount()));
        } else if (paymentTransactionDetails.getPaymentType() == DEPOSIT) {
            userWallet.setFixedDepositAmount(userWallet.getFixedDepositAmount().add(paymentTransactionDetails.getAmount()));
        } else if (paymentTransactionDetails.getPaymentType() == CAUTION) {
            userWallet.setCautionAmount(userWallet.getCautionAmount().add(paymentTransactionDetails.getAmount()));
        } else {
            userWallet.setAmount(userWallet.getAmount().add(paymentTransactionDetails.getAmount()));
        }
        userWalletRepository.saveAndFlush(userWallet);
    }

}
