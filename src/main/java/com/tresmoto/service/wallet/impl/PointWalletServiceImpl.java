package com.tresmoto.service.wallet.impl;

import com.tresmoto.client.CashPointRequest;
import com.tresmoto.repository.RewardPointWalletRepository;
import com.tresmoto.repository.entity.RewardPointWallet;
import com.tresmoto.service.wallet.PointWalletService;
import com.tresmoto.utils.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class PointWalletServiceImpl implements PointWalletService {

    @Autowired
    private RewardPointWalletRepository rewardPointWalletRepository;

    @Autowired
    private ObjectMapperUtils objectMapperUtils;

    @Transactional
    public void processWalletById(CashPointRequest walletRequest) {
        rewardPointWalletRepository.saveAndFlush(
                objectMapperUtils.map(walletRequest, RewardPointWallet.class));
    }
}
