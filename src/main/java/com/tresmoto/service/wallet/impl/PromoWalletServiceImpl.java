package com.tresmoto.service.wallet.impl;

import static com.tresmoto.constants.Status.PENDING;

import com.tresmoto.client.PaymentRequest;
import com.tresmoto.constants.Status;
import com.tresmoto.repository.PromotionalWalletRepository;
import com.tresmoto.repository.entity.PromotionalWallet;
import com.tresmoto.service.wallet.PromoWalletService;
import com.tresmoto.utils.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static com.tresmoto.constants.PaymentConstant.TRES_MOTO;
import static com.tresmoto.constants.Status.SUCCESS;

@Service
public class PromoWalletServiceImpl implements PromoWalletService {

    @Autowired
    private PromotionalWalletRepository promotionalWalletRepository;

    @Autowired
    private ObjectMapperUtils objectMapperUtils;

    @Transactional
    public void savePromotionalWallet(PaymentRequest paymentRequest, String paymentTransactionId) {
        promotionalWalletRepository.saveAndFlush(getPromotionalWallet(paymentRequest, paymentTransactionId));
    }

    public PromotionalWallet getWalletByPaymentTransactionId(String paymentTransactionId) {
        return promotionalWalletRepository.findByPaymentTransactionId(paymentTransactionId);
    }

    public int updateStatusByPaymentTransactionId(Status status, String paymentTransactionId) {
        return promotionalWalletRepository.updatePromotionStatus(status, paymentTransactionId);
    }

    private PromotionalWallet getPromotionalWallet(PaymentRequest paymentRequest, String paymentTransactionId) {
        Date date = new Date();
        PromotionalWallet promotionalWallet = new PromotionalWallet();
        promotionalWallet.setUserId(paymentRequest.getUser().getUuid());
        promotionalWallet.setAmount(paymentRequest.getAmount().getPromotionRequest().getAmount());
        promotionalWallet.setCreatedOn(date);
        promotionalWallet.setCreatedBy(TRES_MOTO);
        promotionalWallet.setStatus(SUCCESS);
        promotionalWallet.setType(paymentRequest.getAmount().getPromotionRequest().getType());
        promotionalWallet.setCode(paymentRequest.getAmount().getPromotionRequest().getCode());
        promotionalWallet.setDescription(paymentRequest.getAmount().getPromotionRequest().getDescription());
        promotionalWallet.setPaymentTransactionId(paymentTransactionId);
        return promotionalWallet;
    }


}
