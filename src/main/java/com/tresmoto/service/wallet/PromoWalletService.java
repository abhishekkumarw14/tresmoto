package com.tresmoto.service.wallet;

import com.tresmoto.client.PaymentRequest;
import com.tresmoto.constants.Status;
import com.tresmoto.repository.entity.PromotionalWallet;

public interface PromoWalletService {


    void savePromotionalWallet(PaymentRequest paymentRequest, String paymentTransactionId);

    PromotionalWallet getWalletByPaymentTransactionId(String paymentTransactionId);

    int updateStatusByPaymentTransactionId(Status status, String paymentTransactionId);
}
