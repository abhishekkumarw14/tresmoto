package com.tresmoto.service.wallet;

import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.repository.entity.UserWallet;

public interface UserWalletService {

    UserWallet findByUserId(String userId);

    void updateUserWallet(PaymentTransactionDetails paymentTransactionDetails);

}
