package com.tresmoto.service.token.impl;

import com.tresmoto.client.TokenResponse;
import com.tresmoto.service.token.PaymentTokenService;
import com.tresmoto.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentTokenServiceImpl implements PaymentTokenService {

    @Autowired
    private UUIDUtils uuidUtils;

    public TokenResponse getPaymentTransactionToken() {
        TokenResponse tokenResponse = new TokenResponse();
        tokenResponse.setTokenId(uuidUtils.getUniquePaymentTransactionToken());
        return tokenResponse;

    }
}
