package com.tresmoto.service.token;

import com.tresmoto.client.TokenResponse;

public interface PaymentTokenService {

    TokenResponse getPaymentTransactionToken();
}
