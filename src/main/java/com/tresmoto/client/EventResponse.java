package com.tresmoto.client;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventResponse {

    @JsonProperty("isSuccessFull")
    private boolean isSuccessFull;

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("baseError")
    private BaseError baseError;
}
