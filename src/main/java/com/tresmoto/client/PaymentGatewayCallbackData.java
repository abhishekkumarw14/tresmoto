package com.tresmoto.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tresmoto.constants.PaymentGatewayType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.MultiValueMap;


@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentGatewayCallbackData {
    private String paymentTransactionId;
    private String gatewayReferenceId;
    private PaymentGatewayType paymentGatewayType;
}
