package com.tresmoto.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tresmoto.constants.*;
import lombok.*;

import java.math.BigDecimal;
import java.math.BigInteger;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentResponse {

    @JsonProperty("baseError")
    private BaseError baseError;

    @JsonProperty("status")
    private Status status;

    @JsonProperty("paymentTransactionId")
    private String paymentTransactionId;

    @JsonProperty("gatewayResponseCode")
    private String gatewayResponseCode;

    @JsonProperty("gatewayResponseMessage")
    private String gatewayResponseMessage;

    @JsonProperty("paymentSource")
    private String paymentSource;

    @JsonProperty("paymentMethod")
    private PaymentMethod paymentMethod;

    @JsonProperty("gatewayTransactionId")
    private String gatewayTransactionId;

    @JsonProperty("gatewayOrderId")
    private String gatewayOrderId;

    @JsonProperty("name")
    private String merchantName;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("amount")
    private BigDecimal amount;

    @JsonProperty("callBackUrl")
    private String callBackUrl;

}
