package com.tresmoto.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tresmoto.constants.EventType;
import com.tresmoto.constants.PaymentGatewayType;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
public class PaymentRefundEventRequest extends EventRequest {


    @JsonProperty("paymentTransactionId")
    private String paymentTransactionId;

}
