package com.tresmoto.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.tresmoto.constants.Status;
import com.tresmoto.constants.WalletType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;


@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class WalletRequest {

    private BigInteger userId;

    private BigDecimal amount;

    private Date startTime;

    private Date endTime;

    private String code;

    private String description;

    private WalletType walletType;

    private Status status;
}
