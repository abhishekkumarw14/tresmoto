package com.tresmoto.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentRefundEventResponse extends EventResponse {

    @JsonProperty("paymentEngineTransactionId")
    private String paymentEngineTransactionId;

}
