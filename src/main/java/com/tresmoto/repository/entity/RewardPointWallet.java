package com.tresmoto.repository.entity;

import com.tresmoto.constants.CashPointType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name = "REWARD_POINT_WALLET")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RewardPointWallet extends AbstractAttributes {

    @Id
    @Column(name = "USER_ID", nullable = false)
    private String userId;

    @Column(name = "AMOUNT", nullable = false)
    private BigDecimal amount;

    @Column(name = "START_TIME", nullable = false)
    private Date startTime;

    @Column(name = "END_TIME", nullable = false)
    private Date endTime;

    @Column(name = "TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private CashPointType type;

    @Column(name = "CODE", nullable = false)
    private String code;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;


}
