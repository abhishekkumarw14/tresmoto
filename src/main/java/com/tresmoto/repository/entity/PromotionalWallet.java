package com.tresmoto.repository.entity;


import com.tresmoto.constants.PromotionType;
import com.tresmoto.constants.Status;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "PROMOTIONAL_WALLET")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PromotionalWallet extends AbstractAttributes {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "USER_ID", nullable = false)
    private String userId;

    @Column(name = "PAYMENT_TRANSACTION_ID", nullable = false)
    private String paymentTransactionId;

    @Column(name = "AMOUNT", nullable = false)
    private BigDecimal amount;

    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private PromotionType type;

    @Column(name = "CODE")
    private String code;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private Status status;


}
