package com.tresmoto.repository.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;

@Entity
@Table(name = "USER_WALLET")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserWallet extends AbstractAttributes {

    @Id
    @Column(name = "USER_ID", nullable = false)
    private String userId;

    @Column(name = "WALLET_AMOUNT", nullable = false, columnDefinition = "Decimal(10,2) default '0.00'")
    private BigDecimal amount;

    @Column(name = "PROMOTION_AMOUNT", nullable = false, columnDefinition = "Decimal(10,2) default '0.00'")
    private BigDecimal promotionalAmount;

    @Column(name = "DUE_AMOUNT", nullable = false, columnDefinition = "Decimal(10,2) default '0.00'")
    private BigDecimal dueAmount;

    @Column(name = "FIXED_DEPOSIT_AMOUNT", nullable = false, columnDefinition = "Decimal(10,2) default '0.00'")
    private BigDecimal fixedDepositAmount;

    @Column(name = "CAUTION_AMOUNT", nullable = false, columnDefinition = "Decimal(10,2) default '0.00'")
    private BigDecimal cautionAmount;

}
