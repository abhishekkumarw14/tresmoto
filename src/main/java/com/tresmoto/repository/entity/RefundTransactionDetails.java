package com.tresmoto.repository.entity;

import com.tresmoto.constants.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Table(name = "REFUND_TRANSACTION_DETAILS")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RefundTransactionDetails extends AbstractAttributes {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "GATEWAY_CODE")
    @Enumerated(EnumType.STRING)
    private PaymentGatewayType paymentGatewayCode;

    @Column(name = "PAYMENT_TRANSACTION_ID", nullable = true)
    private String paymentTransactionId;

    @Column(name = "GATEWAY_REFUND_TRANSACTION_ID", nullable = true)
    private String gatewayRefundTransactionId;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "REFUND_AMOUNT", nullable = false)
    private BigDecimal refundAmount;

    @Column(name = "AMOUNT", nullable = false)
    private BigDecimal amount;


    @Column(name = "INITIATED_BY", nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionInitiator transactionInitiator;

    @Column(name = "PURCHASE_TRANSACTION_DATE")
    private Date purchaseTransactionDate;

//    insert into REFUND_TRANSACTION_DETAILS(ID,GATEWAY_CODE,PAYMENT_TRANSACTION_ID,STATUS,REFUND_AMOUNT,AMOUNT,INITIATED_BY)
//    values(1,'PAYTM','pay_45c78cab-1dab-11ea-8e02-e7495419d43f','PENDING',2,2,'SELF_INITIATED')
}
