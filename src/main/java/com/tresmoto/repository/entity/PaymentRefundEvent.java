package com.tresmoto.repository.entity;

import com.tresmoto.constants.EventType;
import com.tresmoto.constants.PaymentGatewayType;
import com.tresmoto.constants.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "PAYMENT_REFUND_EVENT_HANDLER")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentRefundEvent extends AbstractAttributes {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false, unique = true)
    private Long id;

    @Column(name = "PAYMENT_TRANSACTION_ID", nullable = false)
    private String paymentTransactionId;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE", nullable = false)
    private EventType eventType;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @Column(name = "amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "GATEWAY_CODE")
    @Enumerated(EnumType.STRING)
    private PaymentGatewayType paymentGatewayType;

}
