package com.tresmoto.repository.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tresmoto.constants.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;


@Entity
@Table(name = "PAYMENT_TRANSACTION_DETAILS")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentTransactionDetails extends AbstractAttributes {

    @Id
    @Column(name = "PAYMENT_TRANSACTION_ID", nullable = false)
    private String paymentTransactionId;

    @Column(name = "TENANT_CODE")
    private String tenantCode;

    @Column(name = "USER_ID")
    private String userId;

    @Column(name = "GATEWAY_CODE")
    @Enumerated(EnumType.STRING)
    private PaymentGatewayType paymentGatewayType;

    @Column(name = "PAYMENT_METHOD")
    @Enumerated(EnumType.STRING)
    private PaymentMethod paymentMethod;

    @Column(name = "PAYMENT_SOURCE")
    private String paymentSource;

    @Column(name = "TENANT_TX_ID")
    private String tenantTransactionId;

    @Column(name = "GATEWAY_TRANSACTION_ID")
    private String gatewayTransactionId;

    @Column(name = "PAYMENT_CHANNEL")
    @Enumerated(EnumType.STRING)
    private ChannelType channelType;

    @Column(name = "APP_TYPE")
    @Enumerated(EnumType.STRING)
    private AppType appType;

    @Column(name = "GATEWAY_ORDER_ID")
    private String gatewayOrderId;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "GATEWAY_RESPONSE_CODE")
    private String gatewayResponseCode;

    @Column(name = "GATEWAY_RESPONSE_MESSAGE")
    private String gatewayResponseMessage;

    @Column(name = "AMOUNT", nullable = false)
    private BigDecimal amount;

    @JsonProperty("PROMOTIONAL_AMOUNT")
    private BigDecimal promotionalAmount;

    @Column(name = "INITIATED_BY")
    @Enumerated(EnumType.STRING)
    private TransactionInitiator transactionInitiator;

    @Column(name = "PAYMENT_TYPE")
    @Enumerated(EnumType.STRING)
    private PaymentType paymentType;

}
