package com.tresmoto.repository.entity;


import com.tresmoto.constants.OfferType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "OFFER")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Offer extends AbstractAttributes {

    @Id
    @Column(name = "OFFER_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long offerId;

    @Column(name = "AMOUNT", nullable = false)
    private BigDecimal amount;

    @Column(name = "START_TIME", nullable = false)
    private Date startTime;

    @Column(name = "END_TIME", nullable = false)
    private Date endTime;

    @Column(name = "TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private OfferType type;

    @Column(name = "CODE", nullable = false)
    private String code;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;


}
