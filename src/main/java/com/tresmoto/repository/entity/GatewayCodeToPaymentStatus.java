package com.tresmoto.repository.entity;

import com.tresmoto.constants.PaymentGatewayType;
import com.tresmoto.constants.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "GATEWAY_CODE_TO_PAYMENT_STATUS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GatewayCodeToPaymentStatus extends AbstractAttributes {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", length = 30, unique = true, nullable = false)
    private Long id;

    @Column(name = "TRANSACTION_TYPE",nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @Column(name = "GATEWAY_CODE", nullable = false)
    @Enumerated(EnumType.STRING)
    private PaymentGatewayType gatewayCode;

    @Column(name = "GATEWAY_STATUS_CODE", nullable = false)
    private String gatewayStatusCode;

    @Column(name = "IS_TRANSACTION_VALID", nullable = false)
    private boolean isTransactionValid;

    @Column(name = "IS_TRANSACTION_SUCCESS", nullable = false)
    private boolean isStatusSuccess;

}
