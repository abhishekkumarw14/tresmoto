package com.tresmoto.repository;

import com.tresmoto.repository.entity.GatewayCodeToPaymentStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GatewayCodeToPaymentStatusRepository  extends JpaRepository<GatewayCodeToPaymentStatus, String> {
}
