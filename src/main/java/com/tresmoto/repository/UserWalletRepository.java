package com.tresmoto.repository;

import com.tresmoto.repository.entity.UserWallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserWalletRepository extends JpaRepository<UserWallet, Long> {

    @Transactional(isolation = Isolation.READ_COMMITTED)
    UserWallet findByUserId(String userId);

}
