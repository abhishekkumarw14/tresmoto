package com.tresmoto.repository;

import com.tresmoto.constants.Status;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;
import java.math.BigDecimal;

@Repository
public interface PaymentTransactionDetailsRepository extends JpaRepository<PaymentTransactionDetails, Long> {

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value = "5000")})
    @Query("SELECT pts FROM PaymentTransactionDetails pts WHERE pts.paymentTransactionId=:id")
    PaymentTransactionDetails findByPaymentTransactionIdForUpdate(@Param(value = "id") String id);

    @Transactional(isolation = Isolation.READ_COMMITTED)
    PaymentTransactionDetails findByPaymentTransactionId(String id);


    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("update PaymentTransactionDetails ptd set ptd.status=:status where ptd.id=:id")
    void updatePaymentStatus(@Param(value = "id") Long id, @Param(value = "status") Status status);


    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value = "5000")})
    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Query("SELECT pts FROM PaymentTransactionDetails pts WHERE pts.gatewayOrderId=:id")
    PaymentTransactionDetails findByGatewayCandidateTransactionIdForUpdate(@Param(value = "id") String id);
}
