package com.tresmoto.repository;

import com.tresmoto.constants.Status;
import com.tresmoto.repository.entity.PromotionalWallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PromotionalWalletRepository extends JpaRepository<PromotionalWallet, Long> {

    List<PromotionalWallet> findByUserId(String id);

    PromotionalWallet findByPaymentTransactionId(String paymentTransactionId);

    @Query(value = "update PromotionalWallet pw set pw.status=:status where pw.paymentTransactionId=:paymentTransactionId ")
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    int updatePromotionStatus(@Param(value = "status") Status status, @Param(value = "paymentTransactionId") String paymentTransactionId
    );
}
