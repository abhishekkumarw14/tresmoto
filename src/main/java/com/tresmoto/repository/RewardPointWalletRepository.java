package com.tresmoto.repository;

import com.tresmoto.repository.entity.RewardPointWallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RewardPointWalletRepository extends JpaRepository<RewardPointWallet, Long> {
}
