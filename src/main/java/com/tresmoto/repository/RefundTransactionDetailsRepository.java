package com.tresmoto.repository;

import com.tresmoto.constants.Status;
import com.tresmoto.repository.entity.PaymentRefundEvent;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.repository.entity.RefundTransactionDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

@Repository
public interface RefundTransactionDetailsRepository extends JpaRepository<RefundTransactionDetails, String> {

    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("update RefundTransactionDetails rtd set rtd.status=:status, rtd.gatewayRefundTransactionId=:gatewayRefundId" +
            " ,rtd.refundAmount=:refundAmount where rtd.id=:id")
    void updateRefundStatus(@Param(value = "id") Long id, @Param(value = "status") Status status,
                                   @Param(value = "gatewayRefundId") String gatewayRefundId,
                                   @Param(value = "refundAmount") BigDecimal refundAmount);

    @Transactional(isolation = Isolation.READ_COMMITTED)
    RefundTransactionDetails findOneById(Long id);

}
