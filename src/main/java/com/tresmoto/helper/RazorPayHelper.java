package com.tresmoto.helper;

import com.tresmoto.client.PaymentRefundResponse;
import com.tresmoto.client.PaymentRequest;
import com.tresmoto.client.PaymentResponse;
import com.tresmoto.dto.request.paytm.PaytmRefundResponse;
import com.tresmoto.dto.request.razorpay.*;
import com.tresmoto.repository.entity.PaymentTransactionDetails;

import java.math.BigDecimal;
import java.security.SignatureException;

public interface RazorPayHelper {

    RazorPayOrderRequest createOrderRequest(PaymentRequest paymentRequest, String paymentTransactionId);

    boolean verifyCheckSum(RazorPayB2BCallbackData paymentGatewayCallbackData) throws Exception;

    PaymentResponse getPaymentResponse(RazorPayB2BCallbackData razorPayB2BCallbackData);

    PaymentResponse getPaymentResponse(RazorPayS2SCallbackData razorPayS2SCallbackData, String paymentTransactionId);

    RazorPayRefundRequest createRefundRequest(PaymentTransactionDetails paymentTransactionDetails, BigDecimal amount);

    PaymentResponse getPaymentResponse(PaymentRequest paymentRequest, RazorPayOrderResponse razorPayOrderResponse,
                                       String paymentTransactionId);

    PaymentRefundResponse getPaymentRefundResponse(PaymentTransactionDetails purchaseTransaction,
                                                   RazorPayRefundResponse razorPayRefundResponse);

    PaymentResponse getPaymentResponse(RazorPayPaymentStatusResponse razorPayOrderResponse);


}
