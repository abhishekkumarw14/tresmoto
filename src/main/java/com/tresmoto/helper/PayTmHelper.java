package com.tresmoto.helper;

import com.tresmoto.client.PaymentRefundResponse;
import com.tresmoto.client.PaymentRequest;
import com.tresmoto.client.PaymentResponse;
import com.tresmoto.dto.request.paytm.PayTmTransactionRequest;
import com.tresmoto.dto.request.paytm.PayTmTransactionResponse;
import com.tresmoto.dto.request.paytm.PaytmRefundResponse;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;

import java.math.BigDecimal;
import java.util.TreeMap;


public interface PayTmHelper {

    String getPaymentResponse(PaymentRequest paymentRequest, String transactionId);

    boolean verifyCheckSum(TreeMap<String, String> parameters, String checkSumHash) throws Exception;

    TreeMap<String, String> getPayTmParams(MultiValueMap<String, String> parameters);

    PayTmTransactionRequest getPaymentStatusRequest(String transactionId);

    PaymentResponse getPaymentResponse(PayTmTransactionResponse payTmTransactionResponse, String paymentTransactionId);

    PaymentResponse getPaymentResponse(TreeMap<String, String> parameters, String paymentTransactionId);

    JSONObject createRefundRequest(PaymentTransactionDetails paymentTransactionDetails, BigDecimal refundAmount);

    PaymentRefundResponse getPaymentRefundResponse(PaymentTransactionDetails purchaseTransaction,
                                                   PaytmRefundResponse paytmRefundResponse);

    PaymentResponse getPaymentResponse(PayTmTransactionResponse payTmTransactionResponse);
}
