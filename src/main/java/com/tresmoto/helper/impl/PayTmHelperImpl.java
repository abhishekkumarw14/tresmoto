package com.tresmoto.helper.impl;

import static com.tresmoto.constants.PayTmConstants.*;
import static com.tresmoto.constants.Status.*;

import com.paytm.pg.merchant.CheckSumServiceHelper;
import com.tresmoto.cache.GatewayStatusCodeCache;
import com.tresmoto.client.PaymentRefundResponse;
import com.tresmoto.client.PaymentRequest;
import com.tresmoto.client.PaymentResponse;
import com.tresmoto.constants.*;
import com.tresmoto.dto.request.paytm.*;
import com.tresmoto.exception.PaymentGatewayException;
import com.tresmoto.helper.PayTmHelper;
import com.tresmoto.repository.entity.GatewayCodeToPaymentStatus;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.utils.CommonUtils;
import com.tresmoto.utils.ErrorUtils;
import com.tresmoto.utils.TemplatingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import javax.annotation.PostConstruct;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.TreeMap;

@Component
public class PayTmHelperImpl implements PayTmHelper {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    private CheckSumServiceHelper checkSumServiceHelper;

    @Autowired
    private TemplatingService templatingService;

    @Autowired
    private ErrorUtils errorUtils;

    @Autowired
    private CommonUtils commonUtils;

    @Autowired
    private GatewayStatusCodeCache gatewayStatusCodeCache;


    @PostConstruct
    private void init() {
        this.checkSumServiceHelper = CheckSumServiceHelper.getCheckSumServiceHelper();
    }

    @Value("${paytm.url.callback.b2b}")
    private String payTmCallbackUrl;

    @Value("${paytm.url.transaction}")
    private String transactionURL;

    @Value("${paytm.mid}")
    private String merchantId;

    @Value("${paytm.web-site}")
    private String webSite;

    @Value("${paytm.app-site}")
    private String appSite;

    @Value("${paytm.industry-type}")
    private String industryType;

    @Value("${paytm.m-key}")
    private String merchantKey;

    @Value("${paytm.template-name}")
    private String templateName;

    @Value("${paytm.refund.client-id:C11}")
    private String clientId;

    @Value("${paytm.refund.version:v1}")
    private String version;


    @Override
    public String getPaymentResponse(PaymentRequest paymentRequest, String transactionId) {
        log.debug("PayTmHelperImpl.getPaymentResponse for payTm called and gateway transaction id is {} ", transactionId);
        TreeMap<String, String> parameters = new TreeMap<>();
        parameters.put(MID.name(), merchantId);
        parameters.put(ORDER_ID.name(), transactionId);
        if (ChannelType.WEB == paymentRequest.getPaymentChannelInformation().getChannelType()) {
            parameters.put(CHANNEL_ID.name(), ChannelType.WEB.name());
            parameters.put(WEBSITE.name(), webSite);
        } else {
            parameters.put(CHANNEL_ID.name(), WAP.name());
            parameters.put(WEBSITE.name(), appSite);
        }
        parameters.put(CUST_ID.name(), transactionId);
        parameters.put(MOBILE_NO.name(), paymentRequest.getUser().getMobile());
        parameters.put(EMAIL.name(), paymentRequest.getUser().getEmail());
        parameters.put(TXN_AMOUNT.name(), String.valueOf(
                paymentRequest.getAmount().getAmount().setScale(2, RoundingMode.CEILING)));
        parameters.put(INDUSTRY_TYPE_ID.name(), industryType);
        parameters.put(CALLBACK_URL.name(), commonUtils.getGatewayCallBackUrl(payTmCallbackUrl, transactionId));
        try {
            String payTmChecksum = checkSumServiceHelper.genrateCheckSum(merchantKey, parameters);
            parameters.put(CHECKSUMHASH.name(), payTmChecksum);
            parameters.put(TRANSACTION_URL.name(), transactionURL);
            return templatingService.compileTemplate(templateName, parameters);
        } catch (Exception exp) {
            log.error("exception {} occurred while generating payTm checksum value" +
                    " and payment transaction id is {} ", exp, transactionId);
            throw new PaymentGatewayException(CHECK_SUM_CREATION_FAILURE.getValue());
        }
    }

    @Override
    public PayTmTransactionRequest getPaymentStatusRequest(String transactionId) {

        PayTmTransactionRequest payTmTransactionRequest = new PayTmTransactionRequest();
        TreeMap<String, String> parameters = new TreeMap<>();
        payTmTransactionRequest.setMerchantId(merchantId);
        parameters.put(MID.name(), merchantId);
        payTmTransactionRequest.setOrderId(transactionId);
        parameters.put(ORDER_ID.name(), transactionId);
        try {
            payTmTransactionRequest.setChecksumHash(checkSumServiceHelper.genrateCheckSum(merchantKey, parameters));
            return payTmTransactionRequest;
        } catch (Exception exp) {
            log.error("exception {} occurred while generating payTm checksum value " +
                    "and payment transaction id is {} ", exp, transactionId);
            throw new PaymentGatewayException(CHECK_SUM_CREATION_FAILURE.getValue());
        }
    }


    @Override
    public boolean verifyCheckSum(TreeMap<String, String> parameters, String checkSumHash) throws Exception {
        return checkSumServiceHelper.verifycheckSum(merchantKey, parameters, checkSumHash);

    }

    @Override
    public PaymentResponse getPaymentResponse(PayTmTransactionResponse payTmTransactionResponse, String paymentTransactionId) {
        PaymentResponse paymentResponse = new PaymentResponse();
        paymentResponse.setGatewayTransactionId(payTmTransactionResponse.getTransactionId());
        paymentResponse.setPaymentTransactionId(payTmTransactionResponse.getOrderId());
        paymentResponse.setPaymentMethod(PaymentMethod.WALLET);
        paymentResponse.setPaymentSource(PaymentGatewayType.PAYTM.name());
        if (TXN_SUCCESS.getValue().equalsIgnoreCase(payTmTransactionResponse.getStatus())) {
            paymentResponse.setStatus(SUCCESS);
        } else if (TXN_FAILURE.getValue().equalsIgnoreCase(payTmTransactionResponse.getStatus())) {
            paymentResponse.setBaseError(errorUtils.getBaseError(
                    payTmTransactionResponse.getResponseCode(), payTmTransactionResponse.getResponseMessage()));
            paymentResponse.setStatus(FAILED);
        } else {
            paymentResponse.setStatus(Status.PENDING);
        }
        paymentResponse.setCallBackUrl(commonUtils.getAppCallBackUrl(paymentTransactionId));
        paymentResponse.setGatewayResponseCode(payTmTransactionResponse.getResponseCode());
        paymentResponse.setGatewayResponseMessage(payTmTransactionResponse.getResponseMessage());
        return paymentResponse;
    }

    @Override
    public PaymentResponse getPaymentResponse(TreeMap<String, String> parameters, String paymentTransactionId) {
        PaymentResponse paymentResponse = new PaymentResponse();
        paymentResponse.setGatewayTransactionId(parameters.get(TXN_ID.getValue()));
        paymentResponse.setPaymentTransactionId(parameters.get(ORDER_ID.getValue()));
        paymentResponse.setPaymentMethod(PaymentMethod.WALLET);
        paymentResponse.setPaymentSource(PaymentGatewayType.PAYTM.name());
        paymentResponse.setGatewayResponseCode(parameters.get(RESP_CODE.getValue()));
        paymentResponse.setGatewayResponseMessage(parameters.get(RESP_MSG.getValue()));
        paymentResponse.setCallBackUrl(commonUtils.getAppCallBackUrl(paymentTransactionId));
        return paymentResponse;
    }

    @Override
    public JSONObject createRefundRequest(PaymentTransactionDetails paymentTransactionDetails, BigDecimal refundAmount) {
        try {
            PayTmRefundRequestBody refundRequestBody = getPayTmRefundRequestBody(paymentTransactionDetails, refundAmount);
            JSONObject jsonBody = convertBodyToJsonObject(refundRequestBody);
            PayTmRefundRequestHead refundRequestHead = getPayTmRefundRequestHead(jsonBody);
            return convertToJsonObject(refundRequestHead, jsonBody);
        } catch (Exception exp) {
            log.error("Exception {} occurred createRefundRequest for payment transaction id" +
                    " is {}", exp, paymentTransactionDetails.getPaymentTransactionId());
            throw new PaymentGatewayException(exp.getMessage());
        }
    }

    @Override
    public TreeMap<String, String> getPayTmParams(MultiValueMap<String, String> parameters) {
        TreeMap<String, String> params = new TreeMap<>();
        parameters.forEach((key, value) -> {
            if (CHECKSUMHASH.name().equalsIgnoreCase(key)) {
                log.debug("return checksum value of order id {} is {}",
                        parameters.get(ORDER_ID.getValue()), value.get(0));
            } else {
                params.put(key, value.get(0));
            }
        });
        return params;
    }

    @Override
    public PaymentRefundResponse getPaymentRefundResponse(PaymentTransactionDetails purchaseTransaction,
                                                          PaytmRefundResponse paytmRefundResponse) {
        PaymentRefundResponse paymentRefundResponse = new PaymentRefundResponse();
        paymentRefundResponse.setTotalAmount(purchaseTransaction.getAmount());
        if (null != paytmRefundResponse.getPayTmRefundResponseBody()
                && null != paytmRefundResponse.getPayTmRefundResponseBody().getResultInfo()) {
            paymentRefundResponse.setGatewayRefundId(paytmRefundResponse.getPayTmRefundResponseBody().getRefundId());
            GatewayCodeToPaymentStatus gatewayCodeToPaymentStatus = gatewayStatusCodeCache
                    .getGatewayStatusCodeToPEStatusConfig(TransactionType.REFUND_TRANSACTION, PaymentGatewayType.PAYTM,
                            paytmRefundResponse.getPayTmRefundResponseBody().getResultInfo().getResultCode());
            if (null != gatewayCodeToPaymentStatus) {
                if (!gatewayCodeToPaymentStatus.isTransactionValid()) {
                    paymentRefundResponse.setStatus(INVALID);
                    return paymentRefundResponse;
                } else if (gatewayCodeToPaymentStatus.isStatusSuccess()) {
                    paymentRefundResponse.setStatus(SUCCESS);
                    return paymentRefundResponse;
                }
            }
        }
        return paymentRefundResponse;
    }

    @Override
    public PaymentResponse getPaymentResponse(PayTmTransactionResponse payTmTransactionResponse) {
        PaymentResponse paymentResponse = new PaymentResponse();
        if (TXN_SUCCESS.getValue().equalsIgnoreCase(payTmTransactionResponse.getStatus())) {
            paymentResponse.setStatus(SUCCESS);
        } else if (TXN_FAILURE.getValue().equalsIgnoreCase(payTmTransactionResponse.getStatus())) {
            paymentResponse.setStatus(FAILED);
        }
        return paymentResponse;
    }

    private PayTmRefundRequestBody getPayTmRefundRequestBody(PaymentTransactionDetails paymentTransactionDetails, BigDecimal refundAmount) {
        PayTmRefundRequestBody payTmRefundRequestBody = new PayTmRefundRequestBody();
        payTmRefundRequestBody.setMerchantId(merchantId);
        payTmRefundRequestBody.setOrderId(paymentTransactionDetails.getPaymentTransactionId());
        payTmRefundRequestBody.setRefundAmount(refundAmount.setScale(2, RoundingMode.DOWN));
        payTmRefundRequestBody.setReferenceId(paymentTransactionDetails.getPaymentTransactionId());
        payTmRefundRequestBody.setTransactionId(paymentTransactionDetails.getGatewayTransactionId());
        return payTmRefundRequestBody;
    }

    private PayTmRefundRequestHead getPayTmRefundRequestHead(final JSONObject payTmRefundRequestBody) throws Exception {
        PayTmRefundRequestHead payTmRefundRequestHead = new PayTmRefundRequestHead();
        payTmRefundRequestHead.setClientId(clientId);
        payTmRefundRequestHead.setVersion(version);
        payTmRefundRequestHead.setChecksum(checkSumServiceHelper.genrateCheckSum(merchantKey, payTmRefundRequestBody.toString()));
        return payTmRefundRequestHead;
    }

    private JSONObject convertBodyToJsonObject(final PayTmRefundRequestBody payTmRefundRequestBody) throws Exception {
        final JSONObject bodyParams = new JSONObject();
        bodyParams.put(MID.getValue().toLowerCase(), payTmRefundRequestBody.getMerchantId());
        bodyParams.put(TXN_TYPE.getValue(), payTmRefundRequestBody.getTransactionType());
        bodyParams.put(REF_ORDER_ID.getValue(), payTmRefundRequestBody.getOrderId());
        bodyParams.put(REF_TXN_ID.getValue(), payTmRefundRequestBody.getTransactionId());
        bodyParams.put(REF_REFERENCE_ID.getValue(), payTmRefundRequestBody.getReferenceId());
        bodyParams.put(REFUND_AMOUNT.getValue(), payTmRefundRequestBody.getRefundAmount().toString());
        return bodyParams;
    }

    private JSONObject convertToJsonObject(PayTmRefundRequestHead payTmRefundRequestHead,
                                           JSONObject payTmRefundRequestBody) throws Exception {
        final JSONObject headParams = new JSONObject();
        final JSONObject requestParams = new JSONObject();
        headParams.put(CLIENT_ID.getValue(), payTmRefundRequestHead.getClientId());
        headParams.put(VERSION.getValue(), payTmRefundRequestHead.getVersion());
        headParams.put(SIGNATURE.getValue(), payTmRefundRequestHead.getChecksum());
        requestParams.put(HEAD.getValue(), headParams);
        requestParams.put(BODY.getValue(), payTmRefundRequestBody);
        return requestParams;
    }

}
