package com.tresmoto.helper.impl;

import static com.tresmoto.constants.PaymentMethod.*;
import static com.tresmoto.constants.RazorpayConstants.*;
import static com.tresmoto.constants.Status.*;
import static com.tresmoto.constants.TransactionConstant.*;

import com.tresmoto.cache.GatewayStatusCodeCache;
import com.tresmoto.client.PaymentRefundResponse;
import com.tresmoto.client.User;
import com.tresmoto.client.PaymentRequest;
import com.tresmoto.client.PaymentResponse;
import com.tresmoto.constants.CurrencyConversionConstants;
import com.tresmoto.constants.PaymentGatewayType;
import com.tresmoto.constants.RazorpayConstants;
import com.tresmoto.constants.TransactionType;
import com.tresmoto.dto.request.paytm.PaytmRefundResponse;
import com.tresmoto.dto.request.razorpay.*;
import com.tresmoto.exception.PaymentGatewayException;
import com.tresmoto.helper.RazorPayHelper;
import com.tresmoto.repository.entity.GatewayCodeToPaymentStatus;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.service.security.RazorPaySignatureValidator;
import com.tresmoto.utils.CommonUtils;
import com.tresmoto.utils.ErrorUtils;
import com.tresmoto.utils.TemplatingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;
import java.util.StringJoiner;

@Service
public class RazorPayHelperImpl implements RazorPayHelper {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RazorPaySignatureValidator razorPaySignatureValidator;

    @Autowired
    private ErrorUtils errorUtils;

    @Autowired
    private CommonUtils commonUtils;

    @Autowired
    private TemplatingService templatingService;

    @Value("${razorpay.url.callback.b2b}")
    private String razorPayCallBackUrl;

    @Value("${razorpay.password}")
    private String checksumKey;


    @Value("${razorpay.username}")
    private String keyId;

    @Value("${razorpay.color:#F37254}")
    private String color;

    @Value("${razorpay.company-descrition}")
    private String companyDescription;

    @Value("${razorpay.company-name}")
    private String companyName;

    @Autowired
    private GatewayStatusCodeCache gatewayStatusCodeCache;


    @Override
    public RazorPayOrderRequest createOrderRequest(PaymentRequest paymentRequest, String paymentTransactionId) {
        RazorPayOrderRequest razorPayOrderRequest = new RazorPayOrderRequest();
        razorPayOrderRequest.setAmount(getLowestCommonDenominatorAmountFromInputAmount(paymentRequest.getAmount().getAmount()));
        razorPayOrderRequest.setCurrency(RazorpayConstants.CURRENCY_TYPE);
        razorPayOrderRequest.setReceipt(paymentRequest.getTenant().getTransactionId());
        razorPayOrderRequest.setPaymentCapture(RazorpayConstants.CAPTURE_PAYMENT_YES);
        razorPayOrderRequest.getNotes().put(PAYMENT_TRANSACTION_ID, paymentTransactionId);
        return razorPayOrderRequest;
    }


    @Override
    public PaymentResponse getPaymentResponse(PaymentRequest paymentRequest,
                                              RazorPayOrderResponse razorPayOrderResponse, String paymentTransactionId) {
        if (null == razorPayOrderResponse.getId()) {
            throw new PaymentGatewayException(INVALID_ORDER_ID);
        }
        RazorPayPaymentResponse paymentResponse = new RazorPayPaymentResponse();
        paymentResponse.setGatewayOrderId(razorPayOrderResponse.getId());
        paymentResponse.setCurrency(razorPayOrderResponse.getCurrency());
        paymentResponse.setAmount(razorPayOrderResponse.getAmount());
        setUserDetails(paymentRequest, paymentResponse);
        paymentResponse.setCallBackUrl(commonUtils.getGatewayCallBackUrl(razorPayCallBackUrl, paymentTransactionId));
        paymentResponse.setMerchantName(companyName);
        paymentResponse.setDescription(companyDescription);
        paymentResponse.setKey(keyId);
        paymentResponse.setColor(color);
        paymentResponse.setResponseString(templatingService.compileTemplate("razorpay", paymentResponse));
        return paymentResponse;
    }

    @Override
    public boolean verifyCheckSum(RazorPayB2BCallbackData razorPayB2BCallbackData) throws Exception {
        StringJoiner stringJoiner = new StringJoiner(PIPE_SEPARATOR);
        stringJoiner.add(razorPayB2BCallbackData.getRazorpay_order_id());
        stringJoiner.add(razorPayB2BCallbackData.getRazorpay_payment_id());
        return razorPaySignatureValidator.calculateRFC2104HMAC(stringJoiner.toString(), checksumKey)
                .equals(razorPayB2BCallbackData.getRazorpay_signature());

    }

    @Override
    public PaymentResponse getPaymentResponse(RazorPayB2BCallbackData razorPayB2BCallbackData) {
        PaymentResponse paymentResponse = new PaymentResponse();
        paymentResponse.setGatewayOrderId(razorPayB2BCallbackData.getRazorpay_order_id());
        paymentResponse.setPaymentTransactionId(razorPayB2BCallbackData.getPaymentTransactionId());
        paymentResponse.setGatewayTransactionId(razorPayB2BCallbackData.getRazorpay_payment_id());
        try {
            if (null == razorPayB2BCallbackData.getRazorpay_payment_id()) {
                paymentResponse.setBaseError(errorUtils.getBaseError(INVALID_PAYMENT_ID, INVALID_PAYMENT_ID));
                paymentResponse.setStatus(FAILED);
            } else if (null == razorPayB2BCallbackData.getRazorpay_order_id()) {
                paymentResponse.setBaseError(errorUtils.getBaseError(INVALID_ORDER_ID, INVALID_ORDER_ID));
                paymentResponse.setStatus(FAILED);
            } else if (verifyCheckSum(razorPayB2BCallbackData)) {
                paymentResponse.setStatus(SUCCESS);
            } else {
                paymentResponse.setBaseError(errorUtils.getBaseError(CHECKSUM_VERIFICATION_FAILURE, CHECKSUM_VERIFICATION_FAILURE));
                paymentResponse.setStatus(FAILED);
            }
        } catch (Exception exp) {
            paymentResponse.setStatus(FAILED);
            paymentResponse.setBaseError(errorUtils.getBaseError(VALIDATE_CHECK_SUM, VALIDATE_CHECK_SUM));
        }
        return paymentResponse;
    }

    @Override
    public PaymentResponse getPaymentResponse(RazorPayS2SCallbackData razorPayS2SCallbackData, String paymentTransactionId) {
        PaymentResponse paymentResponse = new PaymentResponse();
        RazorPayPayload razorPayPayload = razorPayS2SCallbackData.getPayload();
        RazorPayEntity razorPayEntity = razorPayPayload.getPayment().getEntity();
        paymentResponse.setGatewayOrderId(razorPayEntity.getOrderId());
        paymentResponse.setPaymentTransactionId(paymentTransactionId);
        if (FAILED.getCode().equalsIgnoreCase(razorPayEntity.getStatus())) {
            paymentResponse.setStatus(FAILED);
        } else if (razorPayEntity.getCaptured()) {
            paymentResponse.setStatus(SUCCESS);
        } else {
            paymentResponse.setStatus(PENDING);
        }
        paymentResponse.setGatewayTransactionId(razorPayEntity.getId());
        paymentResponse.setGatewayResponseCode(razorPayEntity.getErrorCode());
        paymentResponse.setGatewayResponseMessage(razorPayEntity.getErrorDescription());
        if (NET_BANKING.getValue().equalsIgnoreCase(razorPayEntity.getMethod())) {
            paymentResponse.setPaymentMethod(NET_BANKING);
            paymentResponse.setPaymentSource(razorPayEntity.getBank());
        } else if (CARD.getValue().equalsIgnoreCase(razorPayEntity.getMethod())) {
            RazorPayCard razorPayCard = razorPayEntity.getCard();
            paymentResponse.setPaymentMethod(CARD);
            paymentResponse.setPaymentSource(razorPayCard.getIssuer());
        } else if (WALLET.getValue().equalsIgnoreCase(razorPayEntity.getMethod())) {
            paymentResponse.setPaymentMethod(WALLET);
            paymentResponse.setPaymentSource(razorPayEntity.getWallet());
        } else if (UPI.getValue().equalsIgnoreCase(razorPayEntity.getMethod())) {
            paymentResponse.setPaymentMethod(UPI);
            paymentResponse.setPaymentSource(razorPayEntity.getVpa());
        }
        return paymentResponse;
    }

    @Override
    public RazorPayRefundRequest createRefundRequest(PaymentTransactionDetails paymentTransactionDetails, BigDecimal amount) {
        RazorPayRefundRequest razorpayRefundRequest = new RazorPayRefundRequest();
        razorpayRefundRequest.setPaymentId(paymentTransactionDetails.getGatewayTransactionId());
        razorpayRefundRequest.setAmount(getLowestCommonDenominatorAmountFromInputAmount(amount));
        return razorpayRefundRequest;
    }

    @Override
    public PaymentRefundResponse getPaymentRefundResponse(PaymentTransactionDetails purchaseTransaction,
                                                          RazorPayRefundResponse razorPayRefundResponse) {
        PaymentRefundResponse paymentRefundResponse = new PaymentRefundResponse();
        if (razorPayRefundResponse != null && razorPayRefundResponse.getError() == null && razorPayRefundResponse.getRefundId() != null) {
            paymentRefundResponse.setGatewayRefundId(razorPayRefundResponse.getRefundId());
            paymentRefundResponse.setStatus(SUCCESS);
            return paymentRefundResponse;
        }
        RazorPayErrorResponse razorpayErrorResponse = Optional.ofNullable(
                Optional.ofNullable(razorPayRefundResponse).orElse(new RazorPayRefundResponse())
                        .getError()
        ).orElse(new RazorPayErrorResponse());

        GatewayCodeToPaymentStatus gatewayCodeToPaymentStatus = gatewayStatusCodeCache.getGatewayStatusCodeToPEStatusConfig(
                TransactionType.REFUND_TRANSACTION,
                PaymentGatewayType.RAZORPAY,
                razorpayErrorResponse.getCode()
        );
        if (gatewayCodeToPaymentStatus != null && !gatewayCodeToPaymentStatus.isTransactionValid()) {
            paymentRefundResponse.setStatus(INVALID);
        } else {
            paymentRefundResponse.setStatus(FAILED);
        }
        return paymentRefundResponse;
    }

    @Override
    public PaymentResponse getPaymentResponse(RazorPayPaymentStatusResponse razorPayOrderResponse) {
        PaymentResponse paymentResponse = new PaymentResponse();
        if (razorPayOrderResponse != null && !razorPayOrderResponse.getItems().isEmpty()) {
            RazorPayItem razorpayItem = razorPayOrderResponse.getItems().get(0);
            String status = razorpayItem.getStatus();
            if (status.equalsIgnoreCase(RazorpayConstants.RazorpayStatus.CAPTURED.getCode())) {
                paymentResponse.setStatus(SUCCESS);
            } else if (FAILED.name().equalsIgnoreCase(status)) {
                paymentResponse.setStatus(FAILED);
            }
        }
        return paymentResponse;
    }

    private void setUserDetails(PaymentRequest paymentRequest, RazorPayPaymentResponse paymentResponse) {
        if (null != paymentRequest.getUser()) {
            User user = paymentRequest.getUser();
            paymentResponse.setUserEmail(user.getEmail());
            paymentResponse.setUserMobile(user.getMobile());
            paymentResponse.setUserName(user.getFirstName());
        }
    }

    private BigDecimal getLowestCommonDenominatorAmountFromInputAmount(BigDecimal amount) {
        return amount.multiply(CurrencyConversionConstants.INR_CONVERSION_FACTOR).setScale(0, RoundingMode.CEILING);
    }
}
