package com.tresmoto.controller;


import com.tresmoto.client.PaymentRefundEventRequest;
import com.tresmoto.service.payment.PaymentRefundService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping
public class EventController {

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PaymentRefundService paymentRefundService;


    @PostMapping(value = "/payment/refund",
            consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity refundPayment(@RequestBody PaymentRefundEventRequest paymentRefundEventRequest) {
        log.debug("process event for payment transaction id {} and event is {}",
                paymentRefundEventRequest.getPaymentTransactionId(), paymentRefundEventRequest.getEvent());
        paymentRefundService.refundPayment(paymentRefundEventRequest);
        return new ResponseEntity<>(OK);
    }
}
