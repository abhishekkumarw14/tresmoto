package com.tresmoto.controller;

import static com.tresmoto.constants.PaymentGatewayType.*;
import static org.springframework.http.HttpStatus.OK;

import com.tresmoto.client.PaymentResponse;
import com.tresmoto.dto.request.paytm.PayTmCallbackData;
import com.tresmoto.dto.request.razorpay.RazorPayB2BCallbackData;
import com.tresmoto.dto.request.razorpay.RazorPayS2SCallbackData;
import com.tresmoto.factory.service.PaymentGatewayServiceFactory;
import com.tresmoto.service.callback.CallBackService;
import com.tresmoto.utils.TemplatingService;
import com.tresmoto.validator.RazorPayValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@RequestMapping("/callback")
public class CallBackController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PaymentGatewayServiceFactory paymentGatewayServiceFactory;

    @Qualifier("templatingService")
    @Autowired
    private TemplatingService templatingService;

    @Autowired
    private RazorPayValidator razorPayValidator;

    @Autowired
    private CallBackService callBackService;

    @PostMapping(value = "/app/{transactionId}",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.TEXT_HTML_VALUE)
    public ResponseEntity handleAppCallBackResponse(
            @PathVariable(value = "transactionId", required = false) String transactionId) {
        log.info("app callback received for transaction id {}", transactionId);
        return ResponseEntity.status(OK).body(null);

    }

    @RequestMapping(
            value = "/b2b/pay-tm/{transactionId}",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            method = {RequestMethod.GET, RequestMethod.POST},
            produces = MediaType.TEXT_HTML_VALUE
    )
    public ResponseEntity processPayTmGatewayCallBackData(
            @RequestBody MultiValueMap<String, String> parameters, @PathVariable("transactionId") String transactionId) {
        log.info("pay-tm b2b called for transaction Id {} is received and body is {}", transactionId, parameters);
        PayTmCallbackData paymentGatewayCallbackData = new PayTmCallbackData();
        paymentGatewayCallbackData.setParams(parameters);
        paymentGatewayCallbackData.setPaymentTransactionId(transactionId);
        paymentGatewayCallbackData.setPaymentGatewayType(PAYTM);
        PaymentResponse paymentResponse = callBackService.processGatewayCallBackData(paymentGatewayCallbackData);
        return new ResponseEntity<>(templatingService.compileCallbackDataTemplate(paymentResponse), HttpStatus.OK);
    }


    //    @PostMapping(
//            value = "/b2b/razor-pay/{transactionId}",
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    public ResponseEntity<PaymentResponse> handleRazorPayB2BCallbackResponse(
//            @RequestBody RazorPayB2BCallbackData razorpayB2BCallbackData, @PathVariable("transactionId") String transactionId) {
//        log.info("razor-pay b2b called for transaction Id {} is received and body is {}", transactionId, razorpayB2BCallbackData);
//        razorpayB2BCallbackData.setPaymentTransactionId(transactionId);
//        razorpayB2BCallbackData.setPaymentGatewayType(RAZORPAY);
//        PaymentResponse paymentResponse=callBackService.processGatewayCallBackData(razorpayB2BCallbackData);
//        return new ResponseEntity<>(paymentResponse, HttpStatus.OK);
//    }
//
    @PostMapping(
            value = "/b2b/razor-pay/{transactionId}",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<PaymentResponse> handleRazorPayB2BCallbackResponse(
            RazorPayB2BCallbackData razorpayB2BCallbackData, @PathVariable("transactionId") String transactionId) {
        log.info("razor-pay b2b called for transaction Id {} is received and body is {}", transactionId, razorpayB2BCallbackData);
        razorpayB2BCallbackData.setPaymentTransactionId(transactionId);
        razorpayB2BCallbackData.setPaymentGatewayType(RAZORPAY);
        PaymentResponse paymentResponse = callBackService.processGatewayCallBackData(razorpayB2BCallbackData);
        return new ResponseEntity<>(paymentResponse, HttpStatus.OK);
    }

    @PostMapping(
            value = "/s2s/razor-pay",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity handleRazorPayS2SCallbackResponse(
            @RequestHeader(value = "X-Razorpay-Signature", required = false) String signature,
            @RequestBody RazorPayS2SCallbackData razorpayS2SCallbackData, @RequestHeader Map<String, String> headers) {
        log.info("received razor-pay s2s callback headers {} and body is {}", headers, razorpayS2SCallbackData);
        razorPayValidator.validateRazorPayResponse(razorpayS2SCallbackData);
        razorpayS2SCallbackData.setPaymentGatewayType(RAZORPAY);
        razorpayS2SCallbackData.setPaymentTransactionId(razorpayS2SCallbackData.getPayload().getPayment().getEntity().getOrderId());
        callBackService.processGatewayS2SCallBackData(razorpayS2SCallbackData);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
