package com.tresmoto.controller;

import com.tresmoto.client.TokenResponse;
import com.tresmoto.service.token.PaymentTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/token")
public class TokenController {

    @Autowired
    private PaymentTokenService paymentTokenService;

    @GetMapping(value = "/payment")
    public ResponseEntity<TokenResponse> getPaymentTransactionToken() {
        return new ResponseEntity<>(paymentTokenService.getPaymentTransactionToken(), HttpStatus.OK);
    }
}
