package com.tresmoto.controller;

import com.tresmoto.client.PaymentRequest;
import com.tresmoto.client.PaymentResponse;
import com.tresmoto.client.PaymentStatusRequest;
import com.tresmoto.service.payment.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @PostMapping(value = "/pay", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity makePayment(PaymentRequest paymentRequest) {
        return paymentService.makePayment(paymentRequest);

    }

    @PostMapping(value = "/status", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<PaymentResponse> getPaymentStatus(@RequestBody PaymentStatusRequest paymentStatusRequest) {
        return new ResponseEntity<>(paymentService.getPaymentStatus(paymentStatusRequest), HttpStatus.OK);
    }

}
