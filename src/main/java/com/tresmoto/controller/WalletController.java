package com.tresmoto.controller;


import com.tresmoto.client.CashPointRequest;
import com.tresmoto.client.PromotionWalletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/wallet")
public class WalletController {


    @PostMapping(value = "/promotion", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity processPromotionWalletById(PromotionWalletRequest promotionWalletRequest) {
        //walletGatewayService.processWalletById(promotionWalletRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/cash-point", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity processCashPointById(CashPointRequest cashPointRequest) {
     //   walletGatewayService.processWalletById(cashPointRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
