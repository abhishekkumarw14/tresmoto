package com.tresmoto.exception;

import lombok.Getter;
import lombok.Setter;

import static com.tresmoto.constants.ExceptionCode.PAYMENT_REFUND_EXCEPTION;

@Getter
@Setter
public class PaymentRefundException  extends RuntimeException{

    private final String code = PAYMENT_REFUND_EXCEPTION;

    public PaymentRefundException(String message) {
        super(message);
    }
}
