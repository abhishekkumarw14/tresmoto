package com.tresmoto.exception;

import lombok.Getter;

import static com.tresmoto.constants.ExceptionCode.PAYMENT_STATUS_ENQUIRY_EXCEPTION;

@Getter
public class PaymentStatusEnquiryException extends RuntimeException {

    private final String code = PAYMENT_STATUS_ENQUIRY_EXCEPTION;

    public PaymentStatusEnquiryException(String message) {
        super(message);
    }
}
