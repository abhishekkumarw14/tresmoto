package com.tresmoto.exception;

import lombok.Getter;
import lombok.Setter;

import static com.tresmoto.constants.ExceptionCode.PAYMENT_EXCEPTION;

@Getter
@Setter
public class PaymentGatewayException extends RuntimeException {

    private final String code = PAYMENT_EXCEPTION;

    public PaymentGatewayException(String message) {
        super(message);
    }
}
