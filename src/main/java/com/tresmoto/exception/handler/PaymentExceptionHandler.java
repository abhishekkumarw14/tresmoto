package com.tresmoto.exception.handler;

import com.tresmoto.client.BaseError;
import com.tresmoto.exception.OperationNotAllowedException;
import com.tresmoto.exception.PaymentTransactionNotFoundException;
import com.tresmoto.exception.PaymentGatewayException;
import com.tresmoto.exception.RedirectionException;
import com.tresmoto.utils.ErrorUtils;
import com.tresmoto.utils.TemplatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class PaymentExceptionHandler {

    @Autowired
    private TemplatingService templatingService;

    @Autowired
    private ErrorUtils errorUtils;

    @ExceptionHandler(value = RedirectionException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String exceptionHandler(RedirectionException exp) {
        return templatingService.compileCallbackFailureTemplate(exp);
    }

    @ExceptionHandler(value = PaymentGatewayException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public BaseError paymentGatewayExceptionHandler(PaymentGatewayException exp) {
        return errorUtils.getBaseError(exp.getCode(), exp.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public BaseError paymentExceptionHandler(Exception exp) {
        return errorUtils.getBaseError("INTERNAL_SERVER_ERROR", exp.getMessage());
    }

    @ExceptionHandler(value = PaymentTransactionNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public BaseError transactionNotFoundExceptionHandler(PaymentTransactionNotFoundException exp) {
        return errorUtils.getBaseError(exp.getCode(), exp.getMessage());
    }

    @ExceptionHandler(value = OperationNotAllowedException.class)
    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    @ResponseBody
    public BaseError operationNotAllowedExceptionHandler(OperationNotAllowedException exp) {
        return errorUtils.getBaseError(exp.getCode(), exp.getMessage());
    }

}
