package com.tresmoto.exception;

import static com.tresmoto.constants.ExceptionCode.REDIRECTION_EXCEPTION;

public class RedirectionException extends RuntimeException {

    private final String code = REDIRECTION_EXCEPTION;

    public RedirectionException(String message) {
        super(message);
    }
}
