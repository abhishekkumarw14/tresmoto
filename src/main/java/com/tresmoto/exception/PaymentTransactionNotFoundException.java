package com.tresmoto.exception;

import lombok.Getter;
import lombok.Setter;

import static com.tresmoto.constants.ExceptionCode.INVALID_TRANSACTION_ID_EXCEPTION;

@Getter
@Setter
public class PaymentTransactionNotFoundException extends RuntimeException {
    private final String code = INVALID_TRANSACTION_ID_EXCEPTION;

    public PaymentTransactionNotFoundException(String message) {
        super(message);
    }
}
