package com.tresmoto.exception;

import static com.tresmoto.constants.ExceptionCode.OPERATION_NOT_ALLOWED;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OperationNotAllowedException extends RuntimeException {

    private final String code = OPERATION_NOT_ALLOWED;

    public OperationNotAllowedException(String message) {
        super(message);
    }

}
