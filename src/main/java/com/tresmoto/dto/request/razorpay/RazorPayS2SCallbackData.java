package com.tresmoto.dto.request.razorpay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tresmoto.client.PaymentGatewayCallbackData;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RazorPayS2SCallbackData extends PaymentGatewayCallbackData {

    @JsonProperty(value = "entity")
    private String entity;

    @JsonProperty(value = "event")
    private String event;

    @JsonProperty(value = "contains")
    private List<String> contains = new ArrayList<>();

    @JsonProperty(value = "payload")
    private RazorPayPayload payload;

    @JsonProperty(value = "created_at")
    private Long createdAt;
}
