package com.tresmoto.dto.request.razorpay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.List;


@Slf4j
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazorPayPaymentStatusResponse  extends RazorPayGatewayResponse {

    @JsonProperty(value = "count")
    Integer count;

    @JsonProperty(value = "entity")
    String entity;

    @JsonProperty(value = "items")
    List<RazorPayItem> items;
}
