package com.tresmoto.dto.request.razorpay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazorPayRefundRequest {

    @JsonProperty(value = "id")
    private String paymentId;

    @JsonProperty(value = "amount")
   private BigDecimal amount;

    @JsonProperty(value = "notes")
    Map<String, String> notes = new HashMap<>();
}
