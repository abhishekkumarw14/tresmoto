package com.tresmoto.dto.request.razorpay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tresmoto.client.PaymentGatewayCallbackData;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazorPayB2BCallbackData extends PaymentGatewayCallbackData {

    @JsonProperty("razorpay_payment_id")
    private String razorpay_payment_id;

    @JsonProperty("razorpay_order_id")
    private String razorpay_order_id;

    @JsonProperty("razorpay_signature")
    private String razorpay_signature;

    private Map<String, String> notes = new HashMap<>();

    private Map<String, String> error;
}
