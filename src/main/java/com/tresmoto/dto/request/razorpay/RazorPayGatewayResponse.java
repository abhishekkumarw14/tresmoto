package com.tresmoto.dto.request.razorpay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazorPayGatewayResponse {

    @JsonProperty(value = "razorpay_payment_id")
    private String razorpayPaymentId;

    @JsonProperty(value = "razorpay_order_id")
    private String razorpayOrderId;

    @JsonProperty(value = "razorpay_signature")
    private String razorpaySignature;

    @JsonProperty(value = "baseError")
    private RazorPayErrorResponse error;

    @JsonProperty(value = "link")
    private String link;
}
