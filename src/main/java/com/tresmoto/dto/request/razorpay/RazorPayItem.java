package com.tresmoto.dto.request.razorpay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazorPayItem {

    @JsonProperty(value = "id")
    String id;

    @JsonProperty(value = "entity")
    String entity;

    @JsonProperty(value = "amount")
    BigDecimal amount;

    @JsonProperty(value = "currency")
    String currency;

    @JsonProperty(value = "status")
    String status;

    @JsonProperty(value = "amount_refunded")
    BigDecimal amountRefunded;

    @JsonProperty(value = "refund_status")
    String refundStatus;


    @JsonProperty(value = "email")
    String email;

    @JsonProperty(value = "contact")
    String contact;

    @JsonProperty(value = "error_code")
    String errorCode;

    @JsonProperty(value = "error_description")
    String errorDescription;

//    @JsonProperty(value = "notes")
//    Map<String , String> notes = new HashMap<>();

    @JsonProperty(value = "created_at")
    Long createdAt;
}
