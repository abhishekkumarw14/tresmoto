package com.tresmoto.dto.request.razorpay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Map;


@Slf4j
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazorPayOrderResponse extends RazorPayGatewayResponse {

    @JsonProperty(value = "id")
    private String id;

    @JsonProperty(value = "entity")
    private String entity;

    @JsonProperty(value = "amount")
    private BigDecimal amount;

    @JsonProperty(value = "amount_paid")
    private BigDecimal amountPaid;

    @JsonProperty(value = "amount_due")
    private BigDecimal amountDue;

    @JsonProperty(value = "currency")
    private String currency;

    @JsonProperty(value = "receipt")
    private String receipt;

    @JsonProperty(value = "offer_id")
    private BigDecimal offerId;

    @JsonProperty(value = "status")
    private String status;

    @JsonProperty(value = "attempts")
    private Integer attempts;

    @JsonProperty("created_at")
    private Long createdAt;

    @JsonProperty(value = "notes")
    private Map<String, String> notes;
}
