package com.tresmoto.dto.request.razorpay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RazorPayPayload {

    @JsonProperty(value = "payment")
    private RazorPayEntityWrapper payment;

    @JsonProperty(value = "order")
    private RazorPayEntityWrapper order;

    @JsonProperty(value = "invoice")
    private RazorPayEntityWrapper invoice;

    @JsonProperty(value = "subscription")
    private RazorPayEntityWrapper subscription;

    @JsonProperty(value = "dispute")
    private RazorPayEntityWrapper dispute;
}
