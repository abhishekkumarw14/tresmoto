package com.tresmoto.dto.request.razorpay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RazorPayEntity  {

    private String id;
    private String entity;
    private BigDecimal amount;
    private String status;
    @JsonProperty(value = "amount_refunded")
    private BigDecimal amountRefunded;
    @JsonProperty(value = "refund_status")
    private String refundStatus;
    private String method;
    @JsonProperty(value = "order_id")
    private String orderId;

    @JsonProperty(value = "card_id")
    private String cardId;
    private String bank;
    private Boolean captured;
    private String email;
    private String contact;
    private String description;

    @JsonProperty(value = "error_code")
    private String errorCode;

    @JsonProperty(value = "error_description")
    private String errorDescription;
    private BigDecimal fee;
    private BigDecimal tax;
    private Boolean international;
   // private Map<String, String> notes = new HashMap<>();
    private String vpa;
    private String wallet;


    private String currency;

    @JsonProperty(value = "invoice_id")
    private String invoiceId;

    @JsonProperty(value = "created_at")
    private Long createdAt;


    private String receipt;

    @JsonProperty(value = "customer_id")
    private String customerId;

    @JsonProperty(value = "customer_details")
    private RazorPayCustomerDetails customerDetails;

    @JsonProperty(value = "payment_id")
    private String paymentId;

    @JsonProperty(value = "issued_at")
    private Long issuedAt;

    @JsonProperty(value = "paid_at")
    private Long paidAt;

    @JsonProperty(value = "sms_status")
    private Boolean smsStatus;

    @JsonProperty(value = "email_status")
    private Boolean emailStatus;
    private String date;
    private String terms;

    @JsonProperty(value = "short_url")
    private String shortUrl;

    @JsonProperty(value = "view_less")
    private Boolean viewLess;
    private String type;


    @JsonProperty(value = "invoice_number")
    private String invoiceNumber;

    @JsonProperty(value = "expire_by")
    private Long expireBy;

    @JsonProperty(value = "cancelled_at")
    private Long cancelledAt;

    @JsonProperty(value = "partial_payment")
    private Boolean partialPayment;

    @JsonProperty(value = "gross_amount")
    private BigDecimal grossAmount;

    @JsonProperty(value = "amount_paid")
    private BigDecimal amountPaid;

    @JsonProperty(value = "amount_due")
    private BigDecimal amountDue;

    @JsonProperty(value = "billing_start")
    private String billingStart;

    @JsonProperty(value = "billing_end")
    private String billingEnd;

    @JsonProperty(value = "group_taxes_discounts")
    private Boolean groupTaxesDiscounts;

    @JsonProperty(value = "plan_id")
    private String planId;

    @JsonProperty(value = "current_start")
    private Long currentStart;

    @JsonProperty(value = "current_end")
    private Long currentEnd;
    private Integer quantity;
    @JsonProperty(value = "charge_at")
    private Long chargeAt;

    @JsonProperty(value = "start_at")
    private Long startAt;

    @JsonProperty(value = "end_at")
    private Long endAt;

    @JsonProperty(value = "auth_attempts")
    private Integer authAttempts;

    @JsonProperty(value = "total_count")
    private Integer totalCount;

    @JsonProperty(value = "paid_count")
    private Integer paidCount;

    @JsonProperty(value = "customer_notify")
    private Boolean customerNotify;
    private RazorPayCard card;

    @JsonProperty(value = "amount_transferred")
    private BigDecimal amountTransferred;

    @JsonProperty(value = "amount_deducted")
    private BigDecimal amountDeducted;

    @JsonProperty(value = "gateway_dispute_id")
    private String gatewayDisputeId;

    @JsonProperty(value = "reason_code")
    private String reasonCode;

    @JsonProperty("respond_by")
    private Long respondBy;

    private String phase;
}
