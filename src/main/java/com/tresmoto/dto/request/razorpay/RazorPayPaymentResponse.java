package com.tresmoto.dto.request.razorpay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tresmoto.client.PaymentResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazorPayPaymentResponse extends PaymentResponse {

    @JsonProperty("key")
    private String key;

    @JsonProperty("description")
    private String description;

    @JsonProperty("prefill.name")
    private String userName;

    @JsonProperty("prefill.email")
    private String userEmail;

    @JsonProperty("prefill.contact")
    private String userMobile;

    @JsonProperty("notes.fieldname")
    private String notes;

    @JsonProperty("theme.color")
    private String color;

    @JsonProperty(value = "responseString")
    String responseString;

}
