package com.tresmoto.dto.request.razorpay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazorPayRefundResponse extends RazorPayGatewayResponse {

    @JsonProperty(value = "id")
   private String refundId;

    @JsonProperty(value = "entity")
    private String entity;

    @JsonProperty(value = "amount")
    private  BigDecimal amount;

    @JsonProperty(value = "currency")
    private  String currency;

    @JsonProperty(value = "payment_id")
    private String paymentId;

    @JsonProperty(value = "created_at")
    private Long createdAt;

    @JsonProperty(value = "error")
    private RazorPayErrorResponse error;

    @JsonProperty(value = "acquirer_data")
    private RazorPayAcquirerData acquirerData;

    @JsonProperty(value = "receipt")
    private String receipt;

//    @JsonProperty(value = "notes")
//    private Map<String, String> notes = new HashMap<>();
//

}
