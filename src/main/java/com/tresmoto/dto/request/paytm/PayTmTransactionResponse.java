package com.tresmoto.dto.request.paytm;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PayTmTransactionResponse   {

    @JsonProperty("TXNID")
    private String transactionId;

    @JsonProperty("BANKTXNID")
    private String bankTransactionId;

    @JsonProperty("ORDERID")
    private String orderId;

    @JsonProperty("TXNAMOUNT")
    private String taxAmount;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("TXNTYPE")
    private String transactionType;

    @JsonProperty("GATEWAYNAME")
    private String gatewayName;

    @JsonProperty("RESPCODE")
    private String responseCode;

    @JsonProperty("RESPMSG")
    private String responseMessage;

    @JsonProperty("BANKNAME")
    private String bankName;

    @JsonProperty("MID")
    private String merchantId;

    @JsonProperty("PAYMENTMODE")
    private String paymentMode;

    @JsonProperty("REFUNDAMT")
    private String refundAmount;

    @JsonProperty("TXNDATE")
    private String transactionDate;
}
