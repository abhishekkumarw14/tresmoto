package com.tresmoto.dto.request.paytm;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PayTmTransactionRequest  {

    @JsonProperty("MID")
    private String merchantId;

    @JsonProperty("ORDERID")
    private String orderId;

    @JsonProperty("CHECKSUMHASH")
    private String checksumHash;
}
