package com.tresmoto.dto.request.paytm;

import com.tresmoto.client.PaymentGatewayCallbackData;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.MultiValueMap;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class PayTmCallbackData extends PaymentGatewayCallbackData  {

    private MultiValueMap<String, String> params;

}
