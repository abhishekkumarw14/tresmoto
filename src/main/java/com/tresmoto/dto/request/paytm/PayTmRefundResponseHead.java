package com.tresmoto.dto.request.paytm;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class PayTmRefundResponseHead {

    @JsonProperty("version")
    private String version;

    @JsonProperty("clientId")
    private String clientId;

    @JsonProperty("responseTimestamp")
    private String responseTimestamp;

    @JsonProperty("signature")
    private String signature;
}
