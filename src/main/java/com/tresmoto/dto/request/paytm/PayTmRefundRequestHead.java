package com.tresmoto.dto.request.paytm;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class PayTmRefundRequestHead {

    @JsonProperty("clientId")
    private String clientId;

    @JsonProperty("version")
    private String version;

    @JsonProperty("signature")
    private String checksum;
}
