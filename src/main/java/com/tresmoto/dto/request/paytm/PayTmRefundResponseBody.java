package com.tresmoto.dto.request.paytm;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class PayTmRefundResponseBody {

    @JsonProperty("mid")
    private String merchantId;

    @JsonProperty("orderId")
    private String orderId;

    @JsonProperty("refId")
    private String referenceId;

    @JsonProperty("refundAmount")
    private String refundAmount;

    @JsonProperty("refundId")
    private String refundId;

    @JsonProperty("resultInfo")
    private PayTmResultInfo resultInfo;

    @JsonProperty("txnId")
    private String transactionId;

    @JsonProperty("txnTimestamp")
    private String transactionTimestamp;
}
