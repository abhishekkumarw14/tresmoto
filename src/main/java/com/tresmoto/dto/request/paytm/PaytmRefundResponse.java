package com.tresmoto.dto.request.paytm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaytmRefundResponse  {

    @JsonProperty("head")
    private PayTmRefundResponseHead payTmRefundResponseHead;

    @JsonProperty("body")
    private PayTmRefundResponseBody payTmRefundResponseBody;
}
