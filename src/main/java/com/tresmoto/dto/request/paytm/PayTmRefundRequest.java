package com.tresmoto.dto.request.paytm;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class PayTmRefundRequest {

    @JsonProperty("head")
    private PayTmRefundRequestHead payTmRefundRequestHead;

    @JsonProperty("body")
    private PayTmRefundRequestBody payTmRefundRequestBody;
}
