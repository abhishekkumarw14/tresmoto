package com.tresmoto.dto.request.paytm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PayTmRefundRequestBody implements Serializable {

    @JsonProperty("mid")
    private String merchantId;

    @JsonProperty("txnType")
    private String transactionType="REFUND";

    @JsonProperty("orderId")
    private String orderId;

    @JsonProperty("txnId")
    private String transactionId;

    @JsonProperty("refId")
    private String referenceId;

    @JsonProperty("refundAmount")
    private BigDecimal refundAmount;
}
