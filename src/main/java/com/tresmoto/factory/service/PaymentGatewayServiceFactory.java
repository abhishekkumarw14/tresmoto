package com.tresmoto.factory.service;

import com.tresmoto.constants.PaymentGatewayType;
import com.tresmoto.service.gateway.builder.GatewayResponseBuilder;
import com.tresmoto.service.payment.PaymentGatewayService;

public interface PaymentGatewayServiceFactory {

    PaymentGatewayService getPaymentGatewayService(PaymentGatewayType paymentGatewayType);

    GatewayResponseBuilder getGatewayResponseBuilder(PaymentGatewayType paymentGatewayType);
}
