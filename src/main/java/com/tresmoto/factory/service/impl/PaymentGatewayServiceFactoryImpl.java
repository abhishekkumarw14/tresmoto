package com.tresmoto.factory.service.impl;

import com.tresmoto.constants.PaymentGatewayType;
import com.tresmoto.factory.service.PaymentGatewayServiceFactory;
import com.tresmoto.service.gateway.builder.GatewayResponseBuilder;
import com.tresmoto.service.payment.PaymentGatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import static com.tresmoto.constants.PaymentGatewayType.PAYTM;

@Component
public class PaymentGatewayServiceFactoryImpl implements PaymentGatewayServiceFactory {


    @Autowired
    @Qualifier("payTmServiceImpl")
    private PaymentGatewayService payTmServiceImpl;

    @Autowired
    @Qualifier("razorPayServiceImpl")
    private PaymentGatewayService razorPayServiceImpl;

    @Autowired
    @Qualifier("payTmResponseBuilder")
    private GatewayResponseBuilder payTmResponseBuilder;

    @Autowired
    @Qualifier("razorPayResponseBuilder")
    private GatewayResponseBuilder razorPayResponseBuilder;



    @Override
    public PaymentGatewayService getPaymentGatewayService(PaymentGatewayType paymentGatewayType) {
        if (paymentGatewayType == PAYTM) {
            return payTmServiceImpl;
        }
        return razorPayServiceImpl;
    }

    @Override
    public GatewayResponseBuilder getGatewayResponseBuilder(PaymentGatewayType paymentGatewayType) {
        if (paymentGatewayType == PAYTM) {
            return payTmResponseBuilder;
        }
        return razorPayResponseBuilder;
    }
}
