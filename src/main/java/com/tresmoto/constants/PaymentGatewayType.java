package com.tresmoto.constants;

import lombok.ToString;

import java.io.Serializable;

@ToString
public enum PaymentGatewayType implements Serializable {

    RAZORPAY("RAZORPAY"),
    PAYTM("PAYTM");
    String code;

    String value;

    PaymentGatewayType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
