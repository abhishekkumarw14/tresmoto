package com.tresmoto.constants;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;


public interface RazorpayConstants {
    String CURRENCY = "currency";
    String AMOUNT = "amount";
    String ORDER_ID = "order_id";
    String EMAIL = "email";
    String CONTACT = "contact";
    String PAYMENT_INSTRUMENT = "method";

    String CALLBACK_URL = "callback_url";
    String NOTES = "notes";
    String IP = "ip";
    String REFERER = "referer";
    String USER_AGENT = "user_agent";
    String AUTH_TYPE = "auth_type";

    String CARD_NUMBER = "card[number]";
    String CARD_NAME = "card[name]";
    String CARD_EXPIRY_MONTH = "card[expiry_month]";
    String CARD_EXPIRY_YEAR = "card[expiry_year]";
    String CARD_CVV = "card[cvv]";

    String BANK_CODE = "bank";
    String WALLET_CODE = "wallet";

    String VPA_ID = "vpa";
    String FLOW = "flow";
    String INTENT = "intent";
    String PIPE_SEPARATOR = "|";
    String VALIDATE_CHECK_SUM="validate check sum failure";
    String CHECKSUM_VERIFICATION_FAILURE="checksum verification failed";
    String INVALID_ORDER_ID="Invalid RazorPay Order Id";
    String INVALID_PAYMENT_ID="Invalid RazorPay Payment Id";

    String CURRENCY_TYPE = "INR";
    Integer CAPTURE_PAYMENT_YES = 1;
    String X_RAZORPAY_SIGNATURE = "X-Razorpay-Signature";


    @ToString
    @Getter
    @AllArgsConstructor
    public enum RazorpayPaymentInstrument {
        CARD("card"), NET_BANKING("netbanking"), WALLET("wallet"), EMI("emi"), UPI("upi");
        String code;

    }

    @ToString
    @Getter
    @AllArgsConstructor
    public enum RazorpayAuthType {
        OTP("otp"),
        THREE_DS("3ds");

        String code;
    }

    @ToString
    @Getter
    @AllArgsConstructor
    public enum Events {

        @JsonProperty(value = "payment.authorized")
        PAYMENT_AUTHORIZED("payment.authorized"),

        // received just before ORDER_PAID
        @JsonProperty(value = "payment.captured")
        PAYMENT_CAPTURED("payment.captured"),

        @JsonProperty(value = "payment.failed")
        PAYMENT_FAILED("payment.failed"),

        @JsonProperty(value = "payment.dispute.created")
        PAYMENT_DISPUTE_CREATED("payment.dispute.created"),

        // success
        @JsonProperty(value = "order.paid")
        ORDER_PAID("order.paid"),

        @JsonProperty(value = "invoice.paid")
        INVOICE_PAID("invoice.paid"),

        @JsonProperty(value = "invoice.expired")
        INVOICE_EXPIRED("invoice.expired"),

        @JsonProperty(value = "subscription.charged")
        SUBSCRIPTION_CHARGED("subscription.charged");

        String code;

    }

    @ToString
    @Getter
    @AllArgsConstructor
    public enum RazorpayStatus {
        CREATED("created"),
        AUTHORIZED("authorized"),
        // success
        CAPTURED("captured"),
        REFUNDED("refunded"),
        FAILURE("failed");

        String code;
    }

    @ToString
    @Getter
    @AllArgsConstructor
    public enum RazorpayEntityType {
        PAYMENT("payment"),
        ORDER("order"),
        INVOICE("invoice"),
        SUBSCRIPTION("subscription"),
        DISPUTE("dispute");

        String code;
    }

    public static final String ORDER_ID_PATH_PARAM = "order-id";
    public static final String PAYMENT_ID_PATH_PARAM = "razorpay-payment-id";
    public static final String OTP = "otp";
    public static final String EVENT = "event";
    public static final String DESCRIPTION = "description";
}
