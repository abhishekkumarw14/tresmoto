package com.tresmoto.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
public enum PromotionType {

    CASH_BACK("CASHBACK");

    String value;

}
