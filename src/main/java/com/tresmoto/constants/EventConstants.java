package com.tresmoto.constants;

public enum EventConstants {

    INVALID_REQUEST("invalid request"),PAYMENT_ENGINE_EXCEPTION("payment engine exception");

    private String value;

    EventConstants(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
