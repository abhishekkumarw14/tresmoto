package com.tresmoto.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EventType {
    GATEWAY_REFUND, S2S_UPDATE;
}
