package com.tresmoto.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CashPointType {

    CASH_BACK("CASHBACK");

    String value;
}
