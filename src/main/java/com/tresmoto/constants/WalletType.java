package com.tresmoto.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum WalletType {

    PROMOTIONAL_WALLET, CASH_POINT_WALLET, USER_WALLET;

}
