package com.tresmoto.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RideType {

    KEYLESS, SHIFT;
}
