package com.tresmoto.constants;

public enum PaymentType {

    DEPOSIT,CAUTION,PAYMENT_WITH_PROMOTION
}
