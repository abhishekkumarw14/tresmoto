package com.tresmoto.constants;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PaymentMethod {
    CARD("card"), NET_BANKING("netbanking"), UPI("upi"), WALLET("wallet");

    String value;
}
