package com.tresmoto.constants;

import java.math.BigDecimal;

public class CurrencyConversionConstants {
    public static final BigDecimal INR_CONVERSION_FACTOR = new BigDecimal(100);
}