package com.tresmoto.constants;

public interface TransactionConstant {

    String PAYMENT_TRANSACTION_ID = "PAYMENT_TRANSACTION_ID";
    String COLON = ":";
    String BASIC = "Basic ";
    String PAYMENT_TRANSACTION_SEPARATOR = "_";
    String PAYMENT_TRANSACTION_PREFIX = "pay";
    String INVALID_TRANSACTION_ID = "invalid payment transaction id";
}
