package com.tresmoto.constants;

public class TemplateConstants {
    public static final String TENANT_CALLBACK_TEMPLATE = "tenant-callback";
    public static final String CALLBACK_FAILURE_TEMPLATE = "callback-failure";
}
