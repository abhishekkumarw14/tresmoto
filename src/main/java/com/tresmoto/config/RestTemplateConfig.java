package com.tresmoto.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
public class RestTemplateConfig {

    @Value("${razorpay.url.connection-timeout:10000}")
    private long razorpayConnectionTimeOut;

    @Value("${razorpay.url.read-timeout:10000}")
    private long razorpayReadTimeOut;


    @Value("${paytm.url.connection-timeout:5000}")
    private long payTmConnectionTimeOut;

    @Value("${paytm.url.read-timeout:5000}")
    private long payTmReadTimeOut;


    @Bean
    public RestTemplate payTmRestTemplate(RestTemplateBuilder builder) {
        return builder
                .setConnectTimeout(Duration.ofMillis(payTmConnectionTimeOut))
                .setReadTimeout(Duration.ofMillis(payTmReadTimeOut))
                .build();
    }

    @Bean
    public RestTemplate razorPayRestTemplate(RestTemplateBuilder builder) throws Exception {
        return builder
                .setConnectTimeout(Duration.ofMillis(razorpayConnectionTimeOut))
                .setReadTimeout(Duration.ofMillis(razorpayReadTimeOut))
                .build();
    }

    @Bean
    ObjectMapper objectMapper() {
        return new ObjectMapper();
    }


}
