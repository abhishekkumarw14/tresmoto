package com.tresmoto.provider.impl;

import static com.tresmoto.constants.TransactionConstant.*;

import com.tresmoto.client.PaymentRefundResponse;
import com.tresmoto.client.PaymentResponse;
import com.tresmoto.constants.PaymentMethod;
import com.tresmoto.constants.RazorpayConstants;
import com.tresmoto.dto.request.razorpay.*;
import com.tresmoto.exception.PaymentGatewayException;
import com.tresmoto.exception.PaymentRefundException;
import com.tresmoto.exception.PaymentStatusEnquiryException;
import com.tresmoto.provider.RazorPayProvider;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import com.tresmoto.utils.HttpUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

@Component
public class RazorPayProviderImpl implements RazorPayProvider {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${razorpay.url.create-order}")
    private String createOrderURL;

    @Value("${razorpay.url.payment-enquiry}")
    private String paymentEnquiryUrl;

    @Value("${razorpay.url.payment-refund}")
    private String paymentRefundUrl;

    @Value("${razorpay.username}")
    private String username;

    @Value("${razorpay.password}")
    private String password;

    private String authenticationHeader;


    @Autowired
    private RestTemplate razorPayRestTemplate;

    @Autowired
    private HttpUtils httpUtils;

    @PostConstruct
    public void init() {
        StringJoiner stringJoiner = new StringJoiner(COLON);
        stringJoiner.add(username);
        stringJoiner.add(password);
        byte[] encodedAuth = Base64.encodeBase64(stringJoiner.toString().getBytes(StandardCharsets.UTF_8));
        authenticationHeader = BASIC + new String(encodedAuth, StandardCharsets.UTF_8);
    }


    public RazorPayOrderResponse createOrder(RazorPayOrderRequest razorPayOrderRequest, String paymentTransactionId) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticationHeader);
        HttpEntity<RazorPayOrderRequest> httpEntity = new HttpEntity<>(razorPayOrderRequest, httpHeaders);
        try {
            ResponseEntity<RazorPayOrderResponse> response = razorPayRestTemplate
                    .exchange(createOrderURL, HttpMethod.POST, httpEntity, RazorPayOrderResponse.class);
            return response.getBody();
        } catch (Exception exp) {
            log.error("exception while calling {} and error is {} for payment transaction id {} "
                    , createOrderURL, exp.getMessage(), paymentTransactionId);
            throw new PaymentGatewayException(exp.getMessage());
        }
    }

    @Override
    public RazorPayPaymentStatusResponse getPaymentStatus(String razorPayOrderId, String paymentTransactionId) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticationHeader);
        Map<String, String> paramsMaps = new HashMap<>();
        paramsMaps.put(RazorpayConstants.ORDER_ID_PATH_PARAM, razorPayOrderId);
        HttpEntity httpEntity = new HttpEntity<>(null, httpHeaders);
        try {
            ResponseEntity<RazorPayPaymentStatusResponse> razorPayOrderResponse = razorPayRestTemplate.exchange(httpUtils.buildPathUrl(
                    paymentEnquiryUrl, paramsMaps), HttpMethod.GET, httpEntity, RazorPayPaymentStatusResponse.class);
            return razorPayOrderResponse.getBody();
        } catch (Exception exp) {
            log.error("exception while calling {} and error is {} for payment transaction id {} "
                    , paymentEnquiryUrl, exp.getMessage(), paymentTransactionId);
            throw new PaymentStatusEnquiryException(exp.getMessage());
        }
    }

    @Override
    public RazorPayRefundResponse refundPayment(RazorPayRefundRequest razorPayRefundRequest, String paymentTransactionId) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authenticationHeader);
        Map<String, String> map = new HashMap<>();
        map.put(RazorpayConstants.PAYMENT_ID_PATH_PARAM, razorPayRefundRequest.getPaymentId());
        HttpEntity<RazorPayRefundRequest> httpEntity = new HttpEntity<>(razorPayRefundRequest, httpHeaders);
        try {
            ResponseEntity<RazorPayRefundResponse> responseEntity = razorPayRestTemplate.exchange(
                    httpUtils.buildPathUrl(paymentRefundUrl, map),
                    HttpMethod.POST, httpEntity, RazorPayRefundResponse.class);
            return responseEntity.getBody();
        } catch (Exception exp) {
            log.error("exception while calling {} and error is {} for payment transaction id {} "
                    , paymentRefundUrl, exp.getMessage(), paymentTransactionId);
            throw new PaymentRefundException(exp.getMessage());
        }

    }

}
