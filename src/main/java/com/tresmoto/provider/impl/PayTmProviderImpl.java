package com.tresmoto.provider.impl;

import static com.tresmoto.constants.PayTmConstants.CONTENT_TYPE;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tresmoto.dto.request.paytm.PayTmTransactionRequest;
import com.tresmoto.dto.request.paytm.PayTmTransactionResponse;
import com.tresmoto.dto.request.paytm.PaytmRefundResponse;
import com.tresmoto.exception.PaymentRefundException;
import com.tresmoto.exception.PaymentStatusEnquiryException;
import com.tresmoto.provider.PayTmProvider;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


@Component
public class PayTmProviderImpl implements PayTmProvider {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${paytm.url.payment-enquiry}")
    private String paymentEnquiryUrl;

    @Value("${paytm.url.payment-refund}")
    private String paymentRefundUrl;

    @Autowired
    private RestTemplate payTmRestTemplate;

    @Autowired
    private ObjectMapper objectMapper;


    @Override
    public PayTmTransactionResponse getPaymentStatus(PayTmTransactionRequest payTmTransactionRequest) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PayTmTransactionRequest> requestHttpEntity = new HttpEntity<>(payTmTransactionRequest, headers);
        try {
            ResponseEntity<PayTmTransactionResponse> responseEntity = payTmRestTemplate.exchange(
                    paymentEnquiryUrl, HttpMethod.POST, requestHttpEntity, PayTmTransactionResponse.class);
            return responseEntity.getBody();
        } catch (Exception exp) {
            log.error("exception while calling {} and error is {} for payment transaction id {} "
                    , paymentEnquiryUrl, exp.getMessage(), payTmTransactionRequest.getOrderId());
            throw new PaymentStatusEnquiryException(exp.getMessage());
        }
    }


    @Override
    public PaytmRefundResponse refundPayment(JSONObject payTmRefundRequest, String paymentTransactionId) {
        DataOutputStream requestWriter = null;
        InputStream inputStreams = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader responseReader = null;
        try {
            final URL url = new URL(paymentRefundUrl);
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(HttpMethod.POST.name());
            connection.setRequestProperty(CONTENT_TYPE.getValue(), MediaType.APPLICATION_JSON.toString());
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            requestWriter = new DataOutputStream(connection.getOutputStream());
            requestWriter.writeBytes(payTmRefundRequest.toString());
            inputStreams = connection.getInputStream();
            inputStreamReader = new InputStreamReader(inputStreams);
            responseReader = new BufferedReader(inputStreamReader);
            final String responseData = responseReader.readLine();
            return objectMapper.readValue(responseData, PaytmRefundResponse.class);
        } catch (Exception exp) {
            log.error("exception while calling {} and error is {} for payment transaction id {} "
                    , paymentRefundUrl, exp.getMessage(), paymentTransactionId);
            throw new PaymentRefundException(exp.getMessage());
        } finally {
            if (requestWriter != null) {
                IOUtils.closeQuietly(requestWriter);
            }
            if (responseReader != null) {
                IOUtils.closeQuietly(responseReader);
            }
            if (inputStreamReader != null) {
                IOUtils.closeQuietly(inputStreamReader);
            }
            if (inputStreams != null) {
                IOUtils.closeQuietly(inputStreams);
            }
        }
    }


}
