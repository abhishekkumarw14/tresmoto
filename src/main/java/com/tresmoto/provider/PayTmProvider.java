package com.tresmoto.provider;

import com.tresmoto.client.PaymentRefundResponse;
import com.tresmoto.dto.request.paytm.PayTmTransactionRequest;
import com.tresmoto.dto.request.paytm.PayTmTransactionResponse;
import com.tresmoto.dto.request.paytm.PaytmRefundResponse;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import org.json.JSONObject;

public interface PayTmProvider {

    PayTmTransactionResponse getPaymentStatus(PayTmTransactionRequest payTmTransactionRequest) ;

    PaytmRefundResponse refundPayment(JSONObject payTmRefundRequest, String paymentTransactionId);

}
