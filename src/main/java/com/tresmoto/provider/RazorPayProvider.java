package com.tresmoto.provider;


import com.tresmoto.client.PaymentRefundResponse;
import com.tresmoto.client.PaymentResponse;
import com.tresmoto.constants.PaymentMethod;
import com.tresmoto.dto.request.razorpay.*;
import com.tresmoto.repository.entity.PaymentTransactionDetails;
import org.springframework.http.*;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.Map;

public interface RazorPayProvider {

    RazorPayOrderResponse createOrder(RazorPayOrderRequest razorPayOrderRequest, String paymentTransactionId);

    RazorPayPaymentStatusResponse getPaymentStatus(String razorPayOrderId, String paymentTransactionId);

    RazorPayRefundResponse refundPayment(RazorPayRefundRequest razorPayRefundRequest, String paymentTransactionId);

}
